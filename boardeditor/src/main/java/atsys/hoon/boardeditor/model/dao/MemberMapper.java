package atsys.hoon.boardeditor.model.dao;

import atsys.hoon.boardeditor.model.dto.MemberDTO;

public interface MemberMapper {

	int insertMember(MemberDTO member);

	int loginCheck(MemberDTO member);

	String selectEncPassword(String userId);

	String selectEncPassword(MemberDTO member);

	MemberDTO selectMember(MemberDTO member);

}
