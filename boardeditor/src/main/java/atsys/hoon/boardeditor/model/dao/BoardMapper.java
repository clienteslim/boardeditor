package atsys.hoon.boardeditor.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;


import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.dto.BoardFilesDTO;
import atsys.hoon.boardeditor.model.dto.CategoryDTO;

public interface BoardMapper {

	public int selectTotalCount();

	public List<BoardDTO> selectBoardList(@Param("pageInfo") PageInfoDTO pageInfo
			                            , @Param("startRow") int startRow 
			                            ,  @Param("endRow") int endRow);

	public List<BoardDTO> selectPostDetail(@Param("par") String par);
 
	public int modifySelectPost(@Param("par") String par
			                  , @Param("postTitle") String postTitle
			                  , @Param("postContents") String postContents 
			                  , @Param("postDelta") String postDelta
			                  , @Param("categoryName") String categoryName);

	public int boardPost(@Param("postTitle") String postTitle
			           , @Param("userNo") String userNo
			           , @Param("postContents") String postContents
			           , @Param("postDelta") String postDelta
			           , @Param("categoryName") String categoryName);

	public int postDelete(@Param("par") String par);

	public String selectPostDelta(@Param("par") String par);

	public int searchTotalCount(@Param("searchCondition") String searchCondition
			                  , @Param("searchValue") String searchValue);

	public List<BoardDTO> searchBoardList(@Param("pageInfo") PageInfoDTO pageInfo
										, @Param("startRow") int startRow
										, @Param("endRow") int endRow
										, @Param("searchCondition") String searchCondition
										, @Param("searchValue") String searchValue);

	public int uploadFile(@Param("onlyFileName") String onlyFileName
			            , @Param("onlyFileRename") String onlyFileRename
			            , @Param("ext") String ext);

	public List<BoardFilesDTO> selectPostFile(@Param("par") String par);

	public int fileDelete(@Param ("par") String par);

	public int modifyUploadFile(@Param("par") String par
			                  , @Param("onlyFileName") String onlyFileName
			                  , @Param("onlyFileRename") String onlyFileRename
			                  , @Param("ext") String ext);

	public int updatePostViews(@Param("views") int views
			                 , @Param("par") String par);

	public int createCategory(@Param ("categoryName") String categoryName);

	public List<CategoryDTO> selectAllCategory();

	public int deleteCategory(@Param("categoryId") String categoryId);

}
