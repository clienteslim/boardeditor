package atsys.hoon.boardeditor.model.controller.board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("/board/delete")
public class BoardDeleteController {
	
	private final BoardService boardService;
	
	@Autowired
	public BoardDeleteController(BoardService boardService) {
		
		super();
		this.boardService = boardService;
		
	}
	
	
	@PostMapping
	public String postDelete(@RequestParam("par") String par) {
		
		int result = boardService.postDelete(par);
				
		return "redirect:/board";
	}
	
}
