package atsys.hoon.boardeditor.model.controller.board;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.common.page.Pagenation;
import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.dto.CategoryDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("/board")
public class BoardController {

	private final BoardService boardService;
	
	@Autowired
	public BoardController(BoardService boardService) {
		super();
		this.boardService = boardService;
		
	}
	
	
	@RequestMapping(method = { RequestMethod.POST,RequestMethod.GET })
	public String boardList(@RequestParam(value="currentPage", required=false, defaultValue="1") 
							String currentPage
						  ,	@RequestParam(value="categoryId", required=false, defaultValue="0") 
							String categoryId
    					  ,	@RequestParam(value="categoryName", required=false, defaultValue="0") 
	                        String categoryName							
						  , Model model) {
		
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
			          
			
		}
		
		int totalCount = boardService.selectTotalCount();
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		PageInfoDTO pageInfo = Pagenation.getPageInfo(pageNo, totalCount, limit, buttonAmount);
		
		int startRow = pageInfo.getStartRow();		
		
		int endRow = pageInfo.getEndRow();
		
		
		List<BoardDTO> boardList = boardService.selectBoardList(pageInfo, startRow, endRow);
				
		List<CategoryDTO> categoryList = boardService.selectAllCategoryList();
		
		
		model.addAttribute("boardList", boardList);
		
		model.addAttribute("pageInfo", pageInfo);
		
		model.addAttribute("categoryList", categoryList);
		
		return "board/boardMain";
	}
	
	
	
	
}
