package atsys.hoon.boardeditor.model.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;


import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.dto.BoardFilesDTO;
import atsys.hoon.boardeditor.model.dto.CategoryDTO;

public interface BoardService {

	int selectTotalCount();

	List<BoardDTO> selectBoardList(@Param("pageInfo") PageInfoDTO pageInfo, 
			                       @Param("startRow") int startRow, 
			                       @Param("endRow") int endRow);

	List<BoardDTO> selectPostDetail(@Param ("par")String par);

	int modifySelectPost(@Param("par") String par
			, @Param("postTitle") String postTitle
			, @Param("postContents") String postContents
			, @Param("postDelta") String postDelta
			, @Param("categoryName") String categoryName);

	int boardPost(@Param("postTitle") String postTitle
			, @Param("userNo") String userNo
			, @Param("postContents") String postContents
			, @Param ("postDelta") String postDelta
			, @Param("categoryName") String categoryName);

	int postDelete(@Param("par") String par);

	String selectPostDelta(@Param("par") String par);

	int searchTotalCount(@Param("searchCondition") String searchCondition
			           , @Param("searchValue") String searchValue);

	List<BoardDTO> searchBoardList(@Param("pageInfo") PageInfoDTO pageInfo
			                     , @Param("startRow") int startRow
			                     , @Param("endRow") int endRow
			                     , @Param("searchCondition") String searchCondition
			                     , @Param("searchValue") String searchValue);

	int uploadFile(@Param("onlyFileName") String onlyFileName
			     , @Param("onlyFileRename") String onlyFileRename
			     , @Param("ext") String ext);

	List<BoardFilesDTO> selectPostFile(@Param("par") String par);

	int fileDelete(@Param("par") String par);

	int modifyUploadFile(@Param("par") String par
			           , @Param("onlyFileName") String onlyFileName
			           , @Param("onlyFileRename") String onlyFileRename
			           , @Param("ext") String ext);

	int updatePostViews(@Param ("views") int views 
			          , @Param("par") String par);

	int CreateCategory(@Param ("categoryName") String categoryName);

	List<CategoryDTO> selectAllCategoryList();

	int deleteCategory(@Param("categoryId") String categoryId);	

}
