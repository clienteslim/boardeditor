package atsys.hoon.boardeditor.model.service;

import java.util.List;

import org.springframework.stereotype.Service;

import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.model.dao.BoardAdminMapper;
import atsys.hoon.boardeditor.model.dto.BoardAdminDTO;

@Service
public class BoardAdminServiceImpl implements BoardAdminService{
	
	private final BoardAdminMapper mapper; 
	
	public BoardAdminServiceImpl(BoardAdminMapper mapper) {
		
		this.mapper = mapper;
		
	}
	
	
	@Override
	public int selectTotalCount() {
		
		return mapper.selectTotalCount();
	}


	@Override
	public List<BoardAdminDTO> selectBoardAdminList(PageInfoDTO pageInfo, int startRow, int endRow) {
		
		return mapper.selectBoardAdminList(pageInfo, startRow, endRow);
	}



}
