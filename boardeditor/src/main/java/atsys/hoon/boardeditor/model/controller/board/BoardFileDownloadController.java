package atsys.hoon.boardeditor.model.controller.board;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atsys.hoon.boardeditor.model.dto.BoardFilesDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@RestController
public class BoardFileDownloadController {
	
	private final BoardService boardService ;
	
	@Autowired
	public BoardFileDownloadController(BoardService boardService) {
		
		this.boardService = boardService;
		
	}
	
	@PostMapping(value="board/fileDownload")
	public void fileDownload(@RequestParam ("par") String par
			              , HttpServletRequest request
			              , HttpServletResponse response) {
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		String fileName = "";
		
		List<BoardFilesDTO> fileNameDTO = boardService.selectPostFile(par);
		
		/* List안의 DTO를 풀어냄 */
		for(int i=0; i<fileNameDTO.size(); i++) {
			
			BoardFilesDTO str = fileNameDTO.get(i);
			
			String fileRename = str.getFileRename();
			String fileExtension = str.getFileExtension();
			
			fileName = fileRename + fileExtension;
					
		}
		
		/*위에서 추출한 값을 토대로 다운로드 스트림 생성할 파일의 경로 설정*/
		
		String downloadFilePath = filePath + "\\" + fileName;
				
		try {
			byte fileByte[] = org.apache.commons.io.FileUtils.readFileToByteArray(new File(downloadFilePath));
			
			response.setContentType("application/octet-stream");
			response.setContentLength(fileByte.length);
			response.setHeader("Content-Disposition",  "attachment; fileName=\""+URLEncoder.encode(downloadFilePath, "UTF-8")+"\";");
			response.getOutputStream().write(fileByte);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	
	
}
