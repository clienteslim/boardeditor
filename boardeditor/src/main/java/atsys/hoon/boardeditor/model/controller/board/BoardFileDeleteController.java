package atsys.hoon.boardeditor.model.controller.board;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import atsys.hoon.boardeditor.model.dto.BoardFilesDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@RestController
public class BoardFileDeleteController {

	private final BoardService boardService;
	
	@Autowired
	public BoardFileDeleteController(BoardService boardService) {
	
		this.boardService = boardService;
		
	}
	
	/*axios로 첨부파일 삭제*/
	@PostMapping(value="/board/fileDelete")
	public int fileDelete(@RequestBody Map map, HttpServletRequest request) {
		
		/*(for 쿼리) axios에서 보낸 Map에서 parameter를 추출하여 String으로 변경 */
		/* par = userNo */
		String par = map.get("par").toString();
		
		/*upload파일의 경로  설정*/
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		String fileName = "";
		
		/*파일 경로를 DTO형의 List로 추출함*/
		List<BoardFilesDTO> fileNameDTO = boardService.selectPostFile(par);
				
		/* List안의 DTO를 풀어냄 */
		for(int i=0; i<fileNameDTO.size(); i++) {
			
			BoardFilesDTO str = fileNameDTO.get(i);
			
			String fileRename = str.getFileRename();
			String fileExtension = str.getFileExtension();
			
			fileName = fileRename + fileExtension;
					
		}
		
		
		/*위에서 추출한 값을 토대로 삭제할 파일의 경로 설정*/
		
		String DeleteTargetPath = filePath + "\\" + fileName;
		
		/*파일 객체를 생성하여 해당 경로의 파일 삭제*/
		File deleteFile = new File(DeleteTargetPath);
		
		if(deleteFile.exists()) {
			
			deleteFile.delete();
			
			System.out.println("delete Complete!!");			
			
		} else {
						
			System.out.println("delete Failed!");
		}
		
		/* DB상의 파일 경로 삭제*/		
		return boardService.fileDelete(par);
	}
	
}
