package atsys.hoon.boardeditor.model.controller.board;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.dto.BoardFilesDTO;
import atsys.hoon.boardeditor.model.dto.CategoryDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("/board/detail")
public class BoardDetailController {

	private final BoardService boardService;

	@Autowired
	public BoardDetailController(BoardService boardService) {
		super();
		this.boardService = boardService;
	}
	
	
	/* 게시글 내용을 보는 상세 페이지 
	 * + 내용(글자 및 문단 속성, 이미지, 첨부파일) 표시
	 * + 조회수 추가 기능	  
	 * + 첨부파일 표시 기능 
	 * + 파일 다운로드 기능 (HttpServletResponse)
	 * + 기 선택 카테고리 표시 기능 및 카테고리 선택(수정) 기능
	 */
	@GetMapping
	public String boardDetail(@RequestParam(value = "par") String par,
			@RequestParam(value = "title", required = false) String title
			, HttpServletRequest request
			, Model model) {
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		String fileName = "";
		
		List<BoardFilesDTO> fileNameDTO = boardService.selectPostFile(par);
		
		/* List안의 DTO를 풀어냄 */
		for(int i=0; i<fileNameDTO.size(); i++) {
			
			BoardFilesDTO str = fileNameDTO.get(i);
			
			String fileRename = str.getFileRename();
			String fileExtension = str.getFileExtension();
			
			fileName = fileRename + fileExtension;
					
		}
		
		/*위에서 추출한 값을 토대로 다운로드 스트림 생성할 파일의 경로 설정*/
		
		String downloadFilePath = filePath + "\\" + fileName;
		
		
		model.addAttribute("downloadFilePath" , downloadFilePath);
		
		
		// par = dto기준으로 postNo
			
			model.addAttribute("par", par);

			model.addAttribute("title", title);

			List<BoardDTO> boardList = boardService.selectPostDetail(par);
			
			/* 기존의 조회수와 delta값을 List에서 DTO형으로 추출함*/
			/* delta(Quill editor 게시글 정보 객체)*/
			
			int views = 0;
			String postDelta = "";
			
			for(BoardDTO board : boardList) {
				
				views = board.getPostViews()+1;   //조회수를 뽑아서 +1 처리
				postDelta = board.getPostDelta();
				
			}
			
			/* 조회수 변동을 DB상에 반영(update) */
			int result = boardService.updatePostViews(views, par);
			
			List<BoardFilesDTO> boardFile = boardService.selectPostFile(par);
			
			
			/* 카테고리 테이블상의 카테고리 리스트를 가져옴 */
			List<CategoryDTO> categoryList = boardService.selectAllCategoryList();
									
			model.addAttribute("postDelta", postDelta);

			model.addAttribute("boardFile", boardFile);

			model.addAttribute("boardList", boardList);
			
			model.addAttribute("categoryList", categoryList);
		

		return "board/detail";
	}

}
