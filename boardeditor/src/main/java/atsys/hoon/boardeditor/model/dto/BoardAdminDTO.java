package atsys.hoon.boardeditor.model.dto;

public class BoardAdminDTO {
	
	private String boardCode;
	private String boardOrder;
	private String boardName;
	private String writePermission;
	private String commentAvailable;
	private String postReplyAvailable;
	private String categoryAvailable;
	private String accessPermission;	
	private String boardAvailable;
	private String showMainBoard;
	
	
	public BoardAdminDTO() {
		super();
	}


	public BoardAdminDTO(String boardCode, String boardOrder, String boardName, String writePermission,
			String commentAvailable, String postReplyAvailable, String categoryAvailable, String accessPermission,
			String boardAvailable, String showMainBoard) {
		super();
		this.boardCode = boardCode;
		this.boardOrder = boardOrder;
		this.boardName = boardName;
		this.writePermission = writePermission;
		this.commentAvailable = commentAvailable;
		this.postReplyAvailable = postReplyAvailable;
		this.categoryAvailable = categoryAvailable;
		this.accessPermission = accessPermission;
		this.boardAvailable = boardAvailable;
		this.showMainBoard = showMainBoard;
	}


	public String getBoardCode() {
		return boardCode;
	}


	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}


	public String getBoardOrder() {
		return boardOrder;
	}


	public void setBoardOrder(String boardOrder) {
		this.boardOrder = boardOrder;
	}


	public String getBoardName() {
		return boardName;
	}


	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}


	public String getWritePermission() {
		return writePermission;
	}


	public void setWritePermission(String writePermission) {
		this.writePermission = writePermission;
	}


	public String getCommentAvailable() {
		return commentAvailable;
	}


	public void setCommentAvailable(String commentAvailable) {
		this.commentAvailable = commentAvailable;
	}


	public String getPostReplyAvailable() {
		return postReplyAvailable;
	}


	public void setPostReplyAvailable(String postReplyAvailable) {
		this.postReplyAvailable = postReplyAvailable;
	}


	public String getCategoryAvailable() {
		return categoryAvailable;
	}


	public void setCategoryAvailable(String categoryAvailable) {
		this.categoryAvailable = categoryAvailable;
	}


	public String getAccessPermission() {
		return accessPermission;
	}


	public void setAccessPermission(String accessPermission) {
		this.accessPermission = accessPermission;
	}


	public String getBoardAvailable() {
		return boardAvailable;
	}


	public void setBoardAvailable(String boardAvailable) {
		this.boardAvailable = boardAvailable;
	}


	public String getShowMainBoard() {
		return showMainBoard;
	}


	public void setShowMainBoard(String showMainBoard) {
		this.showMainBoard = showMainBoard;
	}


	@Override
	public String toString() {
		return "BoardAdminDTO [boardCode=" + boardCode + ", boardOrder=" + boardOrder + ", boardName=" + boardName
				+ ", writePermission=" + writePermission + ", commentAvailable=" + commentAvailable
				+ ", postReplyAvailable=" + postReplyAvailable + ", categoryAvailable=" + categoryAvailable
				+ ", accessPermission=" + accessPermission + ", boardAvailable=" + boardAvailable + ", showMainBoard="
				+ showMainBoard + "]";
	}




	
}
