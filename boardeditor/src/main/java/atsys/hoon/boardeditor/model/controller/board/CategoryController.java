package atsys.hoon.boardeditor.model.controller.board;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import atsys.hoon.boardeditor.model.dto.CategoryDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
public class CategoryController {
	
	private final BoardService boardService;
	
	@Autowired
	public CategoryController(BoardService boardService) {
		
		this.boardService = boardService;
	}
	
	
	@RequestMapping(value="/board/category", method=RequestMethod.GET)
	public String category(Model model) {
		
		List<CategoryDTO> categoryList = boardService.selectAllCategoryList();
		
		model.addAttribute("categoryList", categoryList);
		
		return "board/category";
	}
	
	
	/*ajax로 말머리 생성 후 변경된 리스트를 다시 DB에서 가져와 출력해준다.*/	
	@ResponseBody
	@RequestMapping(value="/board/createCategory", method=RequestMethod.POST)
	public List<CategoryDTO> createCategory(@ModelAttribute CategoryDTO category) {
		
				
		/*입력 받은 Dto형으로 말머리를 받아온다*/
		String categoryName = category.getCategoryName();
		
		/*category(말머리) 생성*/
		int result = boardService.CreateCategory(categoryName);		
				
		List<CategoryDTO> categoryList = boardService.selectAllCategoryList();
		
		return categoryList;
		
	}
	
	@ResponseBody
	@RequestMapping(value="/board/deleteCategory", method=RequestMethod.POST)
	public List<CategoryDTO> deleteCategory(@RequestParam("categoryId") String categoryId){
		
		
		int result = boardService.deleteCategory(categoryId);
		
		List<CategoryDTO> categoryList = boardService.selectAllCategoryList();
		
		return categoryList;

		
	}
	
	
}
