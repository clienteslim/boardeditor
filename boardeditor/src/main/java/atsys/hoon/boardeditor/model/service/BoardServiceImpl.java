package atsys.hoon.boardeditor.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.model.dao.BoardMapper;
import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.dto.BoardFilesDTO;
import atsys.hoon.boardeditor.model.dto.CategoryDTO;

@Service
public class BoardServiceImpl implements BoardService{
	
	private final BoardMapper mapper;
	
	@Autowired
	public BoardServiceImpl(BoardMapper mapper) {
		super();
		this.mapper = mapper;
	}
	
	@Override
	public int selectTotalCount() {
		
		return mapper.selectTotalCount();
	}

	@Override
	public List<BoardDTO> selectBoardList(PageInfoDTO pageInfo, int startRow, int endRow) {
		
		return mapper.selectBoardList(pageInfo, startRow, endRow);
	}

	@Override
	public List<BoardDTO> selectPostDetail(String par) {
		
		return mapper.selectPostDetail(par);

	}

	@Override
	public int modifySelectPost(String par, String postTitle, String postContents, String postDelta, String categoryName) {
		
		return mapper.modifySelectPost(par, postTitle, postContents, postDelta, categoryName);
	}

	@Override
	public int boardPost(String postTitle, String userNo, String postContents, String postDelta, String categoryName) {
		
		return mapper.boardPost(postTitle, userNo, postContents, postDelta, categoryName);
	}

	@Override
	public int postDelete(String par) {
		
		return mapper.postDelete(par);
	}

	@Override
	public String selectPostDelta(String par) {
		
		return mapper.selectPostDelta(par);
	}

	@Override
	public int searchTotalCount(String searchCondition, String searchValue) {
		
		return mapper.searchTotalCount(searchCondition, searchValue);
	}

	@Override
	public List<BoardDTO> searchBoardList(PageInfoDTO pageInfo, int startRow, int endRow, String searchCondition,
			String searchValue) {
		
		return mapper.searchBoardList(pageInfo, startRow, endRow, searchCondition, searchValue);
	}

	@Override
	public int uploadFile(String onlyFileName, String onlyFileRename, String ext) {
		
		return mapper.uploadFile(onlyFileName, onlyFileRename, ext);
	}

	@Override
	public List<BoardFilesDTO> selectPostFile(String par) {

		return mapper.selectPostFile(par);
	}

	@Override
	public int fileDelete(String par) {
		
		return mapper.fileDelete(par);
	}

	@Override
	public int modifyUploadFile(String par, String onlyFileName, String onlyFileRename, String ext) {
		
		return mapper.modifyUploadFile(par, onlyFileName, onlyFileRename, ext);
	}

	@Override
	public int updatePostViews(int views, String par) {

		return mapper.updatePostViews(views, par);
	}

	@Override
	public int CreateCategory(String categoryName) {
		
		return mapper.createCategory(categoryName);
	}

	@Override
	public List<CategoryDTO> selectAllCategoryList() {
		
		return mapper.selectAllCategory();
	}

	@Override
	public int deleteCategory(String categoryId) {
		
		return mapper.deleteCategory(categoryId);
	}
	
	
}
