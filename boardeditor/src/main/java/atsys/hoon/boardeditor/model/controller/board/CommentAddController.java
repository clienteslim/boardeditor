package atsys.hoon.boardeditor.model.controller.board;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
public class CommentAddController {
	
	private final BoardService boardService;
	
	public CommentAddController(BoardService boardService) {
		
		this.boardService = boardService;
	}
	
	
	@PostMapping("/board/comment/add")
	@ResponseBody
	public int addComment(@RequestBody Map map) {
		
		String userNo = map.get("userNo").toString();
		String postNo = map.get("postNo").toString();
		String commentDelta = map.get("commentDelta").toString();
		
		System.out.println(userNo);
		System.out.println(postNo);
		System.out.println(commentDelta);
		
		
		//int result = boardService.addComment(userNo, postNo, commentDelta);
		
		return 0;
	}
	
	
	
	
}
