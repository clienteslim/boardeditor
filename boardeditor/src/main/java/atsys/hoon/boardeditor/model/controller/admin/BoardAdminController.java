package atsys.hoon.boardeditor.model.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.common.page.Pagenation;
import atsys.hoon.boardeditor.model.dto.BoardAdminDTO;
import atsys.hoon.boardeditor.model.service.BoardAdminService;

@Controller
public class BoardAdminController {
	
	private final BoardAdminService boardAdminService;
	
	@Autowired
	public BoardAdminController(BoardAdminService boardAdminService) {
		
		this.boardAdminService = boardAdminService;
	}
	
	
	@RequestMapping("/admin/boardAdmin")
	public String boardAdmin(@RequestParam(value="currentPage", required=false, defaultValue="1") 
							String currentPage
						  , Model model) {
	
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
			          
			
		}
		
		int totalCount = boardAdminService.selectTotalCount();
		
		
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		PageInfoDTO pageInfo = Pagenation.getPageInfo(pageNo, totalCount, limit, buttonAmount);
		
		int startRow = pageInfo.getStartRow();		
		
		int endRow = pageInfo.getEndRow();
				
		List<BoardAdminDTO> boardAdminList = boardAdminService.selectBoardAdminList(pageInfo, startRow, endRow);
	
		model.addAttribute("boardAdminList", boardAdminList);
		
		model.addAttribute("pageInfo", pageInfo);
		
		return "admin/boardAdmin";
	}
	
	
	
	
	
	
	
	
	@PostMapping("/admin/addBoard")
	@ResponseBody
	public int addBoard(@RequestParam("par") String par) {
		
		System.out.println(par);
		
		return 0;
	}
	
}
