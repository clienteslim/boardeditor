package atsys.hoon.boardeditor.model.controller.board;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import atsys.hoon.boardeditor.model.dto.CategoryDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("/board/write")
public class BoardWriteController {
	
	private final BoardService boardService;
	
	public BoardWriteController(BoardService boardService) {
		
		this.boardService = boardService;
	}
	
	
	@GetMapping
	public String boardWrite(@RequestParam(value="par") String par,
							Model model) {		
		
		model.addAttribute("par", par);
		
		List<CategoryDTO> categoryList = boardService.selectAllCategoryList();
		
		model.addAttribute("categoryList", categoryList);
		
		return "board/write";
	}
	
}
