package atsys.hoon.boardeditor.model.dto;


public class BoardDTO {

	private String postNo;
	private String userNo;
	private String postTitle;
	private String postContents;
	private String postDate;
	private int postViews;
	private String postAvailable;
	private String postDelta;	
	private String categoryName;
	
	
	public BoardDTO() {
		super();
	}


	public BoardDTO(String postNo, String userNo, String postTitle, String postContents, String postDate, int postViews,
			String postAvailable, String postDelta, String categoryName) {
		super();
		this.postNo = postNo;
		this.userNo = userNo;
		this.postTitle = postTitle;
		this.postContents = postContents;
		this.postDate = postDate;
		this.postViews = postViews;
		this.postAvailable = postAvailable;
		this.postDelta = postDelta;
		this.categoryName = categoryName;
	}


	public String getPostNo() {
		return postNo;
	}


	public void setPostNo(String postNo) {
		this.postNo = postNo;
	}


	public String getUserNo() {
		return userNo;
	}


	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}


	public String getPostTitle() {
		return postTitle;
	}


	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}


	public String getPostContents() {
		return postContents;
	}


	public void setPostContents(String postContents) {
		this.postContents = postContents;
	}


	public String getPostDate() {
		return postDate;
	}


	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}


	public int getPostViews() {
		return postViews;
	}


	public void setPostViews(int postViews) {
		this.postViews = postViews;
	}


	public String getPostAvailable() {
		return postAvailable;
	}


	public void setPostAvailable(String postAvailable) {
		this.postAvailable = postAvailable;
	}


	public String getPostDelta() {
		return postDelta;
	}


	public void setPostDelta(String postDelta) {
		this.postDelta = postDelta;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	@Override
	public String toString() {
		return "BoardDTO [postNo=" + postNo + ", userNo=" + userNo + ", postTitle=" + postTitle + ", postContents="
				+ postContents + ", postDate=" + postDate + ", postViews=" + postViews + ", postAvailable="
				+ postAvailable + ", postDelta=" + postDelta + ", categoryName=" + categoryName + "]";
	}



	
	
}
