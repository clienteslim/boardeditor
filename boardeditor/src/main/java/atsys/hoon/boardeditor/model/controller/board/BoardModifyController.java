package atsys.hoon.boardeditor.model.controller.board;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("board/modify")
public class BoardModifyController {
	
	private final BoardService boardService;
	
	public BoardModifyController(BoardService boardService) {
		super();
		this.boardService = boardService;
		
	}
	
	@PostMapping
	public String boardModify(@RequestParam("par") String par
			, @RequestParam("postTitle") String postTitle
			, @RequestParam("postContents") String postContents
			, @RequestParam("postDelta") String postDelta
			, @RequestParam("categoryName") String categoryName
			, @RequestParam(value="uploadFile", required=false) MultipartFile file
			, HttpServletRequest request) {

				
		/* 게시글의 주요정보 및 내용 수정 */
		int result = boardService.modifySelectPost(par, postTitle, postContents, postDelta, categoryName);
		
		
		
		/* 파일 수정 */
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		

		if(file!=null) {
		
			String fileName = file.getOriginalFilename();
			
			String onlyFileName = FilenameUtils.getBaseName(fileName);
			
			String ext = fileName.substring(fileName.length()-4, fileName.length());
			
			
			/*uuid를 생성하여 본래 파일 이름과 합성함.*/
			String uuid = UUID.randomUUID().toString();
			
			String rename = uuid + fileName;
			
			String onlyFileRename = uuid + onlyFileName;
			
			/*file 객체를 생성*/
			File target = new File(filePath, rename);
						
			/*경로 생성*/
			if(!new File(filePath).exists()) {
				
				new File(filePath).mkdirs();
				
			}
						
			/*파일 복사*/
			
			try {
				FileCopyUtils.copy(file.getBytes(), target);
				
				
			} catch (IOException e) {
				e.printStackTrace();
				
			}
		
			/*저장된 업로드파일의 경로를 DB에 저장*/  
			int pathResult = boardService.modifyUploadFile(par, onlyFileName, onlyFileRename, ext);
		}
		
		
		return "redirect:/board";
	}
	
	
}
