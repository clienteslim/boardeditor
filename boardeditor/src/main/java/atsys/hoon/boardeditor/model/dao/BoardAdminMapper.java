package atsys.hoon.boardeditor.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.model.dto.BoardAdminDTO;

public interface BoardAdminMapper {

	int selectTotalCount();

	List<BoardAdminDTO> selectBoardAdminList(@Param("pageInfo") PageInfoDTO pageInfo
								           , @Param("startRow") int startRow
								           , @Param("endRow") int endRow);


}
