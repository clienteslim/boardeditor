package atsys.hoon.boardeditor.model.dto;

import java.sql.Date;

public class BoardFilesDTO {
	
	private String fileGroup;
	private String postNo;
	private String fileOriginName;
	private String fileRename;
	private String fileExtension;
	private Date regDate;
	
	
	public BoardFilesDTO() {
		super();
	}


	public BoardFilesDTO(String fileGroup, String postNo, String fileOriginName, String fileRename, String fileExtension,
			Date regDate) {
		super();
		this.fileGroup = fileGroup;
		this.postNo = postNo;
		this.fileOriginName = fileOriginName;
		this.fileRename = fileRename;
		this.fileExtension = fileExtension;
		this.regDate = regDate;
	}


	public String getFileGroup() {
		return fileGroup;
	}


	public void setFileGroup(String fileGroup) {
		this.fileGroup = fileGroup;
	}


	public String getPostNo() {
		return postNo;
	}


	public void setPostNo(String postNo) {
		this.postNo = postNo;
	}


	public String getFileOriginName() {
		return fileOriginName;
	}


	public void setFileOriginName(String fileOriginName) {
		this.fileOriginName = fileOriginName;
	}


	public String getFileRename() {
		return fileRename;
	}


	public void setFileRename(String fileRename) {
		this.fileRename = fileRename;
	}


	public String getFileExtension() {
		return fileExtension;
	}


	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}


	public Date getRegDate() {
		return regDate;
	}


	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Override
	public String toString() {
		return "BoardFiles [fileGroup=" + fileGroup + ", postNo=" + postNo + ", fileOriginName=" + fileOriginName
				+ ", fileRename=" + fileRename + ", fileExtension=" + fileExtension + ", regDate=" + regDate + "]";
	}
	
	
	
	
}
