package atsys.hoon.boardeditor.model.dto;

public class MemberDTO {
	
	private String userNo;
	private String userId;
	private String userPwd;
	private String userName;
	private String userEmail;
	private String accessPermission;
	
	
	
	public MemberDTO() {
		super();
	}



	public MemberDTO(String userNo, String userId, String userPwd, String userName, String userEmail,
			String accessPermission) {
		super();
		this.userNo = userNo;
		this.userId = userId;
		this.userPwd = userPwd;
		this.userName = userName;
		this.userEmail = userEmail;
		this.accessPermission = accessPermission;
	}



	public String getUserNo() {
		return userNo;
	}



	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}



	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public String getUserPwd() {
		return userPwd;
	}



	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getUserEmail() {
		return userEmail;
	}



	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}



	public String getAccessPermission() {
		return accessPermission;
	}



	public void setAccessPermission(String accessPermission) {
		this.accessPermission = accessPermission;
	}



	@Override
	public String toString() {
		return "MemberDTO [userNo=" + userNo + ", userId=" + userId + ", userPwd=" + userPwd + ", userName=" + userName
				+ ", userEmail=" + userEmail + ", accessPermission=" + accessPermission + "]";
	}




	
	
	
}
