package atsys.hoon.boardeditor.model.service;

import atsys.hoon.boardeditor.model.dto.MemberDTO;

public interface MemberService {

	boolean registMember(MemberDTO member);

	int loginCheck(MemberDTO member);

	String selectEncPassword(String userId);

	MemberDTO loginMember(MemberDTO member);

}
