package atsys.hoon.boardeditor.model.controller.board;


import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("/board/post")
public class BoardPostController {
	
	@Resource(name="uploadPath")
	String uploadPath;
	
	
	private final BoardService boardService;
	
	
	@Autowired
	public BoardPostController(BoardService boardService) {
		
		super();
		this.boardService = boardService;
		
		
	}
	
	@PostMapping
	public String boardPost(@RequestParam("postTitle") String postTitle 
            , @RequestParam("userNo") String userNo
            , @RequestParam("postContents") String postContents
            , @RequestParam("postDelta") String postDelta
            , @RequestParam("categoryName") String categoryName
            , @RequestParam(value="uploadFile", required=false) MultipartFile file            
            , HttpServletRequest request) {
		
		/* 게시글의 주요정보 및 내용 저장*/
		/* 파일이 없을때도 진행해야하므로 조건문 밖에 위치해야함.
		 * 시퀀스 nextval
		 */
		int result = boardService.boardPost(postTitle, userNo, postContents, postDelta, categoryName);
		
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		

		if(file!=null) {
		
			String fileName = file.getOriginalFilename();
			
			String onlyFileName = FilenameUtils.getBaseName(fileName);
			
					
			
			String ext = fileName.substring(fileName.length()-4, fileName.length());
			
			
			
			String uuid = UUID.randomUUID().toString();
			
			String rename = uuid + fileName;
			
			String onlyFileRename = uuid + onlyFileName;
			
			File target = new File(filePath, rename);
			
			
			//경로 생성
			if(!new File(filePath).exists()) {
				
				
				new File(filePath).mkdirs();
				
			}
			
			
			//파일 복사
			
			try {
				FileCopyUtils.copy(file.getBytes(), target);
				
				
			} catch (IOException e) {
				e.printStackTrace();
				
			}
			
			/* 저장된 업로드파일의 경로를 DB에 저장
			 * 파일이 있을때만 진행해야함. 
			 * 시퀀스 currval
			 */
			int pathResult = boardService.uploadFile(onlyFileName, onlyFileRename, ext);
					
		} 
		
		
		return "redirect:/board";
	}
	
	
	
}
