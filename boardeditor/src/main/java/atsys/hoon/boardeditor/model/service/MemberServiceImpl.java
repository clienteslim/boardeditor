package atsys.hoon.boardeditor.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import atsys.hoon.boardeditor.model.dao.MemberMapper;
import atsys.hoon.boardeditor.model.dto.MemberDTO;

@Service
public class MemberServiceImpl implements MemberService{
	
	private final MemberMapper mapper;
	private final BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	public MemberServiceImpl(MemberMapper mapper, BCryptPasswordEncoder passwordEncoder) {
		super();
		this.mapper = mapper;
		this.passwordEncoder = passwordEncoder;
	} 
	
	@Override
	public boolean registMember(MemberDTO member) {		
		
		
		return mapper.insertMember(member) > 0 ? true: false;
	}

	@Override
	public int loginCheck(MemberDTO member) {
		
		int result = mapper.loginCheck(member);
		
		return result;
	}

	@Override
	public String selectEncPassword(String userId) {
		
		String EncPassword = mapper.selectEncPassword(userId);
		
		return EncPassword;
	}

	@Override
	public MemberDTO loginMember(MemberDTO member) {
		
		if(!passwordEncoder.matches(member.getUserPwd(), mapper.selectEncPassword(member))) {
			
			return null;
		}
		
		
		return mapper.selectMember(member);
	}

}
