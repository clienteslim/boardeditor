package atsys.hoon.boardeditor.model.controller.board;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.dto.CategoryDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
public class BoardSearchByCategoryController {
	
	private final BoardService boardService;
	
	@Autowired
	public BoardSearchByCategoryController(BoardService boardService) {
		
		this.boardService = boardService;
		
	}
	
	@ResponseBody
	@RequestMapping(value="/board/searchByCategory", method=RequestMethod.POST )
	public void boardSearchByCategory(@ModelAttribute CategoryDTO category
			) {
		
		System.out.println(category.getCategoryName());
		
		
		//List<BoardDTO> boardList = boardService.searchByCategoryBoardList(pageInfo, startRow, endRow, searchCondition, searchValue);
		
		//return boardList;
	}
	
}
