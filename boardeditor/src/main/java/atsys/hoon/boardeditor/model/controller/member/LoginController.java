package atsys.hoon.boardeditor.model.controller.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import atsys.hoon.boardeditor.model.dto.MemberDTO;
import atsys.hoon.boardeditor.model.service.MemberService;

@Controller
@SessionAttributes("loginMember")
@RequestMapping("/login")
public class LoginController {
	
	private final MemberService memberService;
	private final BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	public LoginController(MemberService memberService, BCryptPasswordEncoder passwordEncoder) {
		this.memberService = memberService;
		this.passwordEncoder = passwordEncoder;
	}
	
	@PostMapping
	public String login(@ModelAttribute MemberDTO member, Model model
			           , RedirectAttributes rttr) {
				
		int result = memberService.loginCheck(member);
		
		String EncPassword = memberService.selectEncPassword(member.getUserId());
		
		String inputPassword = member.getUserPwd();
		
		boolean pwdMatches = passwordEncoder.matches(inputPassword, EncPassword);
		
		
		if( (result == 1) && (pwdMatches == true) ) {
			
			model.addAttribute("loginMember", memberService.loginMember(member));			
		}
		
		
		return "redirect:/";
	}
	
	
	
	
}
