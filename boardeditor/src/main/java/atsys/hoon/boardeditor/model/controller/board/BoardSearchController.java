package atsys.hoon.boardeditor.model.controller.board;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import atsys.hoon.boardeditor.common.page.PageInfoDTO;
import atsys.hoon.boardeditor.common.page.Pagenation;
import atsys.hoon.boardeditor.model.dto.BoardDTO;
import atsys.hoon.boardeditor.model.service.BoardService;

@Controller
@RequestMapping("board/search")
public class BoardSearchController {

	private final BoardService boardService;
	
	@Autowired
	public BoardSearchController(BoardService boardService) {
	
		this.boardService = boardService;
	}
	
	
	@GetMapping
	public String boardSearch(@RequestParam(value="currentPage", required=false, defaultValue="1") 
							  String currentPage
							, @RequestParam ("searchCondition") String searchCondition
							, @RequestParam ("searchValue") String searchValue
						    , Model model) {
		
		
		System.out.println(searchCondition + " , " + searchValue);
		
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
			          
			
		}
		
		int totalCount = boardService.searchTotalCount(searchCondition, searchValue);
		
		System.out.println("totalCount : " + totalCount);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		PageInfoDTO pageInfo = Pagenation.getPageInfo(pageNo, totalCount, limit, buttonAmount);
		
		int startRow = pageInfo.getStartRow();		
		
		int endRow = pageInfo.getEndRow();
		
		
		List<BoardDTO> boardList = boardService.searchBoardList(pageInfo, startRow, endRow, searchCondition, searchValue);
		
		System.out.println(boardList);
		
		model.addAttribute("boardList", boardList);
		model.addAttribute("pageInfo", pageInfo);
		
		
		return "board/boardMain";
	}
	
}
