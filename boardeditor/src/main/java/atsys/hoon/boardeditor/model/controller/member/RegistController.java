package atsys.hoon.boardeditor.model.controller.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import atsys.hoon.boardeditor.model.dto.MemberDTO;
import atsys.hoon.boardeditor.model.service.MemberService;

@Controller
@RequestMapping("member/regist")
public class RegistController {
	
	private final BCryptPasswordEncoder passwordEncoder;
	private final MemberService memberService;
	
	@Autowired              
	public RegistController(MemberService memberService, BCryptPasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
		this.memberService = memberService;
	}
	
	@GetMapping
	public String regist() {
		
		return "member/regist";
	}
	
	@PostMapping
	public String registProcess(@ModelAttribute MemberDTO member, BCryptPasswordEncoder passwordEncoder,
			RedirectAttributes rttr) {
		
		String inputId = member.getUserId();
		
		String pwd = member.getUserPwd();
		
		member.setUserPwd(passwordEncoder.encode(pwd));
		
		
		memberService.registMember(member);
		
		rttr.addFlashAttribute("message", "회원 가입에 성공하셨습니다.");
		
		return "redirect:/";
	}
	
}
