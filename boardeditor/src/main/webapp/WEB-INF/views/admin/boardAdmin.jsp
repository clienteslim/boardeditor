<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>


<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<style>

		#header{		
			width:1920px;
			height:180px;
		}
		
		#section{
			border : solid 1px gray;
		}
				
		#btnArea{
	   		display: flex;
	   	    justify-content:flex-end;
	   	    align-itmes:center;
	   	    margin-top:20px;
	   	    margin-bottom:20px;
		
		}
		
		#boardArea{
	   	    display: flex;
	   	    justify-content:center;
	   	    align-itmes:center;
		}
		
		#adminBoard{
	   		width:900px;
	   		height: auto;
	   		border: solid 1px;
	   		border-collapse: collapse;
	   		table-layout:fixed;
	   		text-align:center;				
		}
		
		table#adminBoard th, table#adminBoard td{
   			border: solid 1px;
   		}
		
	</style>

</head>
<body>
	
		<div id="header">
		
		<jsp:include page="../common/header.jsp"/>
	
		</div>
		
		
		<div id="section">
			
			<div id="btnArea">
				<button class="btn btn-secondary mx-5" type="button" onclick="addBoard();">게시판 추가</button>
			</div>
			
			
			<div id="boardArea">
										
				
				<table id="adminBoard">
				
					<tr>
						<th>index test</th>
						<th>desc test</th>
						<th>no</th>
						<th>게시판명</th>
						<th>글쓰기 권한</th>
						<th>댓글</th>
						<th>답글</th>
						<th>말머리 생성</th>
						<th>접근 권한</th>
						<th>메인 출력</th>
					</tr>
				
				<c:forEach items="${boardAdminList}" var="boardList" varStatus="status">	
					<tr>
						<td><c:out value="${status.index+1 }"/></td>
						<td><c:out value="${fn:length(boardAdminList)-status.index}"/></td>
						<td><c:out value="${boardList.boardOrder }"/></td>
						<td><c:out value="${boardList.boardName }"/></td>

						<td>
						
						<select class="threeOption">
							
						</select>						
						<c:out value="${boardList.writePermission }"/>
						
						
						</td>
						
						<td>
						
						<c:out value="${boardList.commentAvailable }"/>
						
						</td>
						
						<td>
						
						<c:out value="${boardList.postReplyAvailable }"/>
						
						</td>
						
						<td>
						
						<c:out value="${boardList.categoryAvailable }"/>
						
						</td>
						
						<td>
						
						<c:out value="${boardList.accessPermission }"/>
						
						</td>
						<td><c:out value="${boardList.showMainBoard }"/></td>
						
					</tr>
				</c:forEach>	
				</table>
			</div>
		</div>
		
			

		
		</section>
		


	<script>
	
	
		const $threeOptions = document.querySelectorAll('.threeOption'); 
	
		console.log($threeOptions);
		
			for(const $threeOption of $threeOptions){
			
				for(var i=0; i<3; i++){
					
		  		 let option = document.createElement('option');
		  		 
		  		 option.setAttribute('value', 'test');
		  		 option.text = 'test';
		  		 
		  		 $threeOption.append(option);		  		 
		  		 
				}
				
				
			};
		
	
	
		const addBoard = ()=>{
			
			
			
			
			
			
			/* $.ajax({
				type:"POST",
				url: "${pageContext.servletContext.contextPath}/admin/addBoard",
				data : {
					
					par : 
					
				} ,
				success : function(data){
					
					
				}
				
				
			});	 */
			
		}
	
	</script>



</body>



</html>