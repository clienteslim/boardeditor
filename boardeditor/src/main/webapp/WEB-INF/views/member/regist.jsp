<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="/boardeditor/resources/css/regist.css">
</head>


<body>

	<div id="header">
	<jsp:include page="../common/header.jsp"/>
	</div>
	
	<div id="section">
		
		<form action="${pageContext.servletContext.contextPath}/member/regist" method="post">
			<div id="registForm">
		        <fieldset id="registFieldSet">
		            
		            <legend>회원 가입</legend>
		        
		            <table id="registTable">
		                <tr>
		                    <td class="pdlTd"><span>ID</span></td>
		                    <td class="pdlTd2"><input type="text" id="userId" name="userId" required>
		                    <div id="idCheck"></div></td>		                    
		                </tr>
		                <tr>
		                    <td class="pdlTd"><span>비밀번호</span></td>
		                    <td class="pdlTd2"><input type="password" name="userPwd" required></td>
		                </tr>
		                <tr>
		                    <td class="pdlTd"><span>이름</span></td>
		                    <td class="pdlTd2"><input type="text" name="userName" required></td>
		                    
		                </tr>
		                <tr>
		                    <td class="pdlTd"><span>Email</span></td>
		                    <td class="pdlTd2"><input type="email" name="userEmail" required></td>
		                </tr>
		                <tr>
		                    <td colspan="2" id="buttonTd">
		                        <button id="regBtn" type="submit">가입하기</button>   
		                     
		                    </td>
		                </tr>
		
		            </table>                       
		            
		        </fieldset>
	        </div>
    </form>

	

	</div>
	
	<div id="footer">
	
	</div>
	
	
	<script>
		
	
	const $userId = document.querySelector('#userId');
	 
	 const $idCheck = document.querySelector('#idCheck');
	 
	 const $regBtn = document.querySelector('#regBtn');
		
	 $idCheck.textContent = "영문자와 숫자 5~19자"
	 
	 console.log($userId); 
	 
	 $userId.addEventListener('input', (e)=>{
		 
		 const userIdInput = e.target.value;
	     
		 var idRegex = /^[a-zA-Z0-9]{5,19}$/g;
		 
		 
		 if(userIdInput == ''){
			 
			 $idCheck.textContent = "영문자와 숫자 5~19자";
			 
			 $regBtn.removeAttribute('disabled');
			 
		 } else if (idRegex.test(userIdInput)){
				
			 $idCheck.textContent = "알맞은 형식";
			 
			 $regBtn.removeAttribute('disabled'); 
			 
		 } else if(!idRegex.test(userIdInput)){
				
				$idCheck.textContent = "id 형식에 맞지 않습니다.";

				console.log("id 형식에 맞지 않습니다.");
				
				$regBtn.setAttribute('disabled','true'); 
		 } 
		 
	 });
		 
		 
	
	</script>



</body>
</html>