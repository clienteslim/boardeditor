<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
		#header{		
		width:1920px;
		height:180px;
		
		}	
	
		#section{
		border : solid 1px gray;
		}
		
		#boardArea{
   	    display: flex;
   	    justify-content:center;
   	    align-itmes:center;
   	    
   		}
   		
   		#postArea{
   		display: flex;
   	    justify-content:center;
   	    align-itmes:center;  		  		
   		}
   		
   		#postContents{
   			resize:none;
   		}
		
		#buttonArea{
   		display: flex;
   	    justify-content:center;
   	    align-itmes:center;
 	  	}
 	  	
 	  	#centerBtn{
 	  		padding-left:50px;
 	  		padding-right:10px;
 	  	}
 	  	
 	  	#editor{
 	  	
 	  		width:900px;
 	  		height:300px;
 	  	}
	
	
        .resizable{
	        display : inline-block;
	        resize:both;
	        overflow:hidden;
	        line-height: 0;
        }

        .resizable img {
            width: 100%;
            height: 100%;
            }


 	  	
.ql-snow .ql-Table-Input .ql-picker-options {
    padding: 3px 5px;
    width: 152px;
}

.ql-snow .ql-Table-Input .ql-picker-item {
    border: 1px solid transparent;
    float: left;
    height: 16px;
    margin: 2px;
    padding: 0px;
    width: 16px;
    background: lightsteelblue;
}

.ql-snow .ql-Table-Input .ql-picker-item:hover, .ql-snow .ql-Table-Input .ql-picker-item.ql-picker-item-highlight {
    border-color: #000;
    background: steelblue;
}

.ql-snow .ql-Table-Input.ql-picker {
    width: 28px;
}

.ql-snow .ql-Table-Input.ql-picker .ql-picker-label {
    padding: 2px 4px;
}
 	  	
</style>

<!-- jQuery (for quill-better-table)  -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


<!-- Quill editor (+ better-table)  -->

<script src="https://cdn.quilljs.com/2.0.0-dev.3/quill.js"></script>
<script src="https://unpkg.com/quill-better-table@1.2.8/dist/quill-better-table.min.js"></script>
<link type="text/css" href="https://cdn.quilljs.com/2.0.0-dev.3/quill.snow.css" rel="stylesheet">
<link type="text/css" href="https://unpkg.com/quill-better-table@1.2.7/dist/quill-better-table.css" rel="stylesheet"/>

<!-- font-awesome (CustomButton Icon) -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">

<!-- KaTex (formula) -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js"></script>

<body>

	<div id="header">
		
		<jsp:include page="../common/header.jsp"/>
	
	</div>
	
	
		<c:set var="par" value = "${par}"/>
	
	<div id="section">
	
		<div id="boardArea">
		 	
		 	
			 	<form method="POST" name="form" enctype="multipart/form-data">
	        	
		        <div>
		            
		            <input type="hidden" name="par" value="${par}">
		            <input type="hidden" name="userNo" value="${loginMember.userNo}">	            
		        </div>
	
			        <table id="postArea">
				        <tr>
			        		<td>말머리</td>
			        		
			        	</tr>
		        	
			        	<tr>
			        		
			        		<td>
				        		<select name="categoryName">
				        				<option>말머리 선택</option>
		
			        			
									
			        			<c:if test="${empty categoryList}" >
				        				<option>말머리가 없어요</option>
				        				<option>말머리를 만들어주세요</option>
			        			</c:if>	
			        					        			
			        			
			        			<c:if test="${!empty categoryList}"> 
			        			
				        			<c:forEach items="${categoryList}" var="category">
				        				<option value="${category.categoryName}">
				        				   <c:out value="${category.categoryName}"/>
				        				 </option>
				        			</c:forEach>
			        			</c:if>	
			        			
			        			</select>
				        	</td>	
			        		
			        	</tr>
			        	
			        	
			            <tr>
			                <td>제목</td>
			            </tr>

						<tr>
					        <td><input type="text" name="postTitle" required></td>
					    </tr>
						
						<tr>
			                <td>내용</td>
			            </tr>
			            
			            <tr>
			                <td>			                	
			                	<div id="editor"></div>
			                </td>
			            </tr>
						
						
						<tr>
			                <td>
			               	 	<input type="hidden" id="postContents" name="postContents" value="${board.postContents}">
			               		<input type="hidden" id="postDelta" name="postDelta" value="${board.postDelta}">
			                </td>
			            </tr>
			            
					 </table>	
					 
					<div id="buttonArea">
						<table>
							<tr>
								<td><button class="btn btn-secondary" onclick="javascript: form.action='${pageContext.servletContext.contextPath}/board'">뒤로</button></td>
								<td id="centerBtn">
									<button class="btn btn-primary" onclick="javascript: form.action='${pageContext.servletContext.contextPath}/board/post'">작성 완료</button>
								</td>	
							</tr>
			
						</table>
		        	
		        	
			</div> <!-- buttonArea 끝 -->
	
		</form>
	
		</div> <!-- boardArea 끝 -->
	
	</div> <!-- section 끝 -->
	
	
	<script>
	
	var curQuillDiv = $("#editor");
	
	
	Quill.register({
	  'modules/better-table': quillBetterTable
	}, true);
		
	var tableOptions = [];

	var maxRows = 7;
	var maxCols = 7;

	for (let r = 1; r <= maxRows; r++) {
		  for (let c = 1; c <= maxCols; c++) {
		    tableOptions.push('newtable_' + r + '_' + c);
		  }
	};
	
	const $buttonArea = document.querySelector('#buttonArea');
	
		
	let imgCount = 0;
	
	const imageHandler = ()=>{		
		
		const input = document.createElement('input');
		
		input.setAttribute('type', 'file');
		input.setAttribute('accept', 'image/*');
		input.click();
		
		input.addEventListener('change', (e)=>{
			
			var files = e.target.files;
			
			var fileReader = new FileReader();
			
			fileReader.readAsDataURL(e.target.files[0]);
			
			const range = quill.getSelection();
			
			fileReader.onload = function(e){				
				
				quill.insertEmbed(range, 'image', e.target.result);
								
			}
					
		});
						
	}
	
	
	
	
	var toolbarOptions = [
		
		[{ 'header': [1, 2, 3, 4, 5, 6, false]}],        
        [{ 'color': [] }, 'bold', 'italic', 'underline', 'strike'],        
        [{ 'align': [] }],
        ['blockquote', 'code-block'],        
        [{'list': 'ordered'}, {'list': 'bullet'}, {'list': 'check'}],
        ['image'],
		['fileUpload'],
		['formula'],		
        ['clean'],
        [{'Table-Input': tableOptions}]
        
	];
	
	
	var options = {
			modules: {	
			    toolbar: {
			    	container :toolbarOptions,
			    	handlers:{
			    		'image': imageHandler
			    	}
			    },	
			    table: false,
			    'better-table': {
			      operationMenu: {
			        items: {
			          unmergeCells: {
			            text: 'Unmerge cells'
			          }
			        },
			        color: {
			          colors: ['green', 'red', 'yellow', 'blue', 'white'],
			          text: 'Background Colors:'
			        }
						}
					},
			    keyboard: {
			      bindings: quillBetterTable.keyboardBindings
			    }
			  },
			  theme: 'snow',
			  readOnly : false		
	}
	
	
	
		
	var Block = Quill.import('blots/block');	                    
    Block.tagName = 'DIV';
    Quill.register(Block, true); 
	
	
	var quill = new Quill(curQuillDiv[0], options);
		
	/*에디터 내부의 텍스트 변화를 감지한다.사진 첨부 or 사진을 다른 div로 이동해도 감지 가능.*/
    quill.on('text-change', function(){
        
        let $div = document.querySelectorAll('div');

            for(var i = 0 ; i<$div.length; i++){

                var item = $div.item(i);
                $(item).find('img').parent().attr("class","resizable"); 
                // img태그를 포함한 div에 resizable 클래스 속성 부여
                
                $(item).find('br').parent().removeAttr("class");                
                // img 태그가 없는 div는 resizable 클래스를 없앰 (여백기호의 속성만 남겨야함)
            } 
    });  
		
	/* 테이블 생성 관련 JQuery 코드 */
	var curTableIconSpan = curQuillDiv.parent().find('span.ql-Table-Input.ql-picker')[0].childNodes[0];
	curTableIconSpan.innerHTML = "<svg style=\"right: 4px;\" viewbox=\"0 0 18 18\"> <rect class=ql-stroke height=12 width=12 x=3 y=3></rect> <rect class=ql-fill height=2 width=3 x=5 y=5></rect> <rect class=ql-fill height=2 width=4 x=9 y=5></rect> <g class=\"ql-fill ql-transparent\"> <rect height=2 width=3 x=5 y=8></rect> <rect height=2 width=4 x=9 y=8></rect> <rect height=2 width=3 x=5 y=11></rect> <rect height=2 width=4 x=9 y=11></rect> </g> </svg>";
	var curTableCellIconSpans = $(curTableIconSpan.parentNode.childNodes[1]).children();
	curTableCellIconSpans.click((function(){
	  var curQuillBetterTable = quill.getModule('better-table');
	  var curQuillToolbar = quill.getModule('toolbar');
	  return function() {
	    var curRowIndex = Number(this.dataset.value.substring(9).split('_')[0]);
	    var curColIndex = Number(this.dataset.value.substring(9).split('_')[1]);
	    curQuillBetterTable.insertTable(curRowIndex, curColIndex);
	    // The following two lines have been added, thinking that it would fix the issue of keeping the icon in blue color.  However Quill keeps adding the classes back, so this fix doesn't work.
	    $(this).parent().parent().find(".ql-selected").removeClass("ql-selected");     	 $(this).parent().parent().find(".ql-active").removeClass("ql-active");
	  };
	})());
	curTableCellIconSpans.hover(function(){
	  var curRowIndex = Number(this.dataset.value.substring(9).split('_')[0]);
	  var curColIndex = Number(this.dataset.value.substring(9).split('_')[1]);
	  $(this).parent().children().each((function() {
	    var curRowIndex1 = curRowIndex;
	    var curColIndex1 = curColIndex;
	    return function() {
	      var curRowIndex2 = Number(this.dataset.value.substring(9).split('_')[0]);
	      var curColIndex2 = Number(this.dataset.value.substring(9).split('_')[1]);
	      if (curRowIndex2 <= curRowIndex1 && curColIndex2 <= curColIndex1) {
	        $(this).addClass("ql-picker-item-highlight");
	      }
	    };
	  })());


	}, function(){
	  $(this).parent().children().removeClass("ql-picker-item-highlight");
	});
	//////////테이블 생성 관련  JQuery 코드 끝
	
	
	const $editorForm = document.querySelector('#editor');
	
	/* 커스텀 버튼 생성 */
	var customBtn = document.querySelector('.ql-fileUpload');
	
	var icon = document.createElement('i');
	
	icon.setAttribute('class','fas fa-file-upload');
	
	customBtn.append(icon);  
	   
	
	
	
	/*업로드 파일 확장자 체크*/
	const checkFile=(f)=>{
		
		var file = f.files;
		
		if(!/\.(gif|jpg|jpeg|png|mp4|wma|mp3|ogg|mkv|pdf|hwp|doc|docx|xls|xlsx|txt|csv|ppt|zip|7z|rar|alz)$/i.test(file[0].name)) {
			alert('허용된 확장자만 가능합니다.\n\n현재 파일 : ' + file[0].name)
		} // 체크를 통과했다면 종료.
		else {
			return		
		};
		
		f.outerHTML = f.outerHTML;
		
	};
	
	
	/* 파일 첨부는 한개만 가능하도록 flag 설정*/
	var count = 0;
	
	customBtn.addEventListener('click', function(){
		
		if(count == 0){
		
		var input = document.createElement('input');
		
		var acceptUploadType = '.gif,.jpg,.jpeg,.png,.mp4,.wma,.mp3,.ogg,.mkv,.pdf,.hwp,.doc,.docx,.xls,.xlsx,.txt,.csv,.ppt,.zip,.7z,.rar,.alz';
		
		input.setAttribute('type','file');
		input.setAttribute('accept', acceptUploadType);
		input.setAttribute('id','uploadFile');
		input.setAttribute('name','uploadFile');
		input.setAttribute('onchange','checkFile(this)');
		
		
		$editorForm.append(input);
		
		
		
		input.click();
		
		count++;
		
		} else {
			
			alert('파일은 한개만 첨부 가능');
		}
	})
	
	////////////// 커스텀 버튼 생성 끝  	
	
	
	
	const form = document.querySelector('form');
	
	/*form 태그의 정보들을 submit하기 이전에 처리*/
	form.onsubmit = function(){
		
		var postDelta = document.querySelector('input[name=postDelta]');
		
		postDelta.value = JSON.stringify(quill.getContents());	
	}
	
	</script>
	
</body>
</html>