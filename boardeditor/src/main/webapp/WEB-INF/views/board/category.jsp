<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


<!-- font-awesome (CustomButton Icon) -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">

<!-- bootStrap -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>



</head>

<body>


<div class="container">
	
	<input type="text" id="categoryName" name="categoryName">

		<div class="mx-4 mt-4">
			<button class="btn btn-primary" onclick="createCategory()">+ 생성</button>
			<button class="btn btn-danger">취소</button>
		</div>
	
</div>


<div id="listBody" class="container mt-5">
	
	
	<label class="form-label">말머리 목록 </label>
	
	<div id="listDiv" class="container mt-3">	
	
	
		<c:forEach items="${categoryList}" var="category" varStatus="status">

			<button class="btn btn-danger" onclick="deleteCategory(`${category.categoryId}`)">X</button>
				<span class="categoryNameList${fn:length(categoryList)-status.index}"><c:out value="${category.categoryName}" /><br></span>
				<hr>
			
		</c:forEach>
	


	</div>
	

</div>


</body>

	<script>
		

		
		function createCategory(){
		
		
		const $categoryName = document.querySelector('#categoryName');
		
		var categoryNameValue = $categoryName.value;
		
			if(categoryNameValue==''){
				
				alert("값을 입력한 후 생성해야합니다.")
				return;
			}
		
			
			/* JQuery를 사용한 ajax 내부에서는 
			 * 바닐라 자바스크립트가 인식되지 않으므로 JQuery문법을 사용해야함.
			 */
			$.ajax({
				
				type:"POST",
				url:"${pageContext.servletContext.contextPath}/board/createCategory",
				data:{
					categoryName : categoryNameValue
				},
				
				
				/*ajax 성공시 data -> category 목록을 다시 받아옴.*/
				success : function(data){

					
						/* span 태그를 다시 생성하여 */
						var spanTag = document.createElement('span');
						
						/* 카테고리 리스트 순서대로 번호를 붙이고*/
						spanTag.setAttribute('class', 'categoryNameList'+ (data.length+1) );
						
						/* div에 span태그를 붙인 다음*/
						$('#listDiv').prepend(spanTag);
						
						/* span태그마다 각 번호에 해당하는 카테고리 이름 값을 넣어준다.*/
						$('.categoryNameList'+ (data.length+1) ).html(data[0].categoryName);
						
						/*한 줄로 나열되지 않도록 br태그를 후행하여 넣어주면 block요소처럼 된다.*/
						$('.categoryNameList'+ (data.length+1) ).after("<hr/>");
						
						
						/*삭제 버튼을 각 말머리에 붙여준다. */
						var deleteBtn = document.createElement('button');
						deleteBtn.setAttribute('class', 'btn btn-danger');
						deleteBtn.setAttribute('onclick', 'deleteCategory(`' + data[0].categoryId + '`)'); 
																					//백틱을 문자열로 취급해야하므로 유의
						$(deleteBtn).html("X");
						
						$(spanTag).prepend(deleteBtn);
						
						
						
						/* 팝업창(자식창) => 부모창의 값 변경 
						 * 팝업에서 말머리 생성하는 즉시 반영 
						 * option태그를 생성하여 말머리 검색용 select에 추가한다.*/
						
						var optionTag = document.createElement('option');
						
						optionTag.setAttribute('value', data[0].categoryName);
						
						optionTag.setAttribute('class', 'optionList'+ (data.length+1) );
						
						$(optionTag).html(data[0].categoryName);
						
						$(opener.document).find(".defaultOption").after(optionTag);
				}
				
			});
	};
	
	
	
	const deleteCategory=(par)=>{
		
		
		if( !confirm("정말 삭제하시겠습니까?") ){
			
			return;
		}
		
		const $categoryName = document.querySelector('#categoryName');
		
		var categoryNameValue = $categoryName.value;
		
		console.log(par);
		
		$.ajax({
			
			type : "POST",
			url : "${pageContext.servletContext.contextPath}/board/deleteCategory",
			data : {
				categoryId : par
			},
			
			success:function(data){
				
				console.log(data);
				
				$('#listDiv').remove();
				
				var listDiv = document.createElement('div');
				
				listDiv.setAttribute('class','container mt-3');
				listDiv.setAttribute('id','listDiv');
				
				$('#listBody').append(listDiv);
				
				const absoluteLength = data.length;
				
				
				for(var i=0 ; i<data.length; i++){
					
					
					var spanTag = document.createElement('span');
					spanTag.setAttribute('class', "categoryNameList" + (absoluteLength - i) );
					$(spanTag).html(data[i].categoryName);
					$(spanTag).append("<hr>");
					$('#listDiv').append(spanTag);

					
					var deleteBtn = document.createElement('button');
					deleteBtn.setAttribute('class', 'btn btn-danger');
					deleteBtn.setAttribute('onclick', 'deleteCategory(`'+data[i].categoryId+'`)');
					$(deleteBtn).html("X");
					
					$(spanTag).prepend(deleteBtn)
				}
				
			}
			
		});
		
		
	}
	
	
	
	</script>

</html>