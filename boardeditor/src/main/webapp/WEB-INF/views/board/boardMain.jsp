<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
	#header{		
		width:1920px;
		height:180px;
	}
	
	#section{
		border : solid 1px gray;
	}
	
	#boardArea{
   	    display: flex;
   	    justify-content:center;
   	    align-itmes:center;
   	    
   	}
   	
   	table#freeBoard{
   		width:900px;
   		height: auto;
   		border: solid 1px;
   		border-collapse: collapse;
   		table-layout:fixed;
   		text-align:center;   		 		
   	}
   	
   	#pagingArea {
   		display: flex;
   	    justify-content:center;
   	    align-itmes:center;
   	    margin-top:20px;
   	    margin-bottom:20px;
   	    
   	}
   	
   	table#freeBoard th, table#freeBoard td{
   		border: solid 1px;
   	}
   	
   	
   	#searchAndWriteArea{
   		display: flex;
   	    justify-content:center;
   	    align-itmes:center;
   	    margin-top:20px;
   	    margin-bottom:20px;
   	}
   	
   	
   	#writeBtn{
   		margin-left : 30px;
   	}
   	
   	.titleBtn{
   		background :none;
   		border: none;
   		color:blue;
   		text-decoration:underline;   		
   	}
   	
   	.titleBtn:hover{
   		color:purple;
   	}   	
   	
</style>

<!-- bootStrap -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

<!-- jQuery (for quill-better-table)  -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


</head>
<body>

	<div id="header">
		
		<jsp:include page="../common/header.jsp"/>
	
	</div>

	<div id="section">
	
	
			<div id="searchAndWriteArea">	
		
				<button class="btn btn-success" type="button" onclick="openPopup()">말머리 추가</button>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				
				<form action="${pageContext.servletContext.contextPath}/board/search">	
									
					 <select name="searchCondition">
						 <option value="postNo">글번호</option>
						 <option value="postTitle">제목</option>
						 <option value="userNo">글쓴이</option>
					 </select>
					
					
					<input name="searchValue" type="text" placeholder="검색어 입력"> <button type="submit">검색</button>
		
				</form>
					
				<form action="${pageContext.servletContext.contextPath}/board/write" method="get" onsubmit="return loginCheck();">
					<div>
						<input type="hidden" name="par" value="">
						<button id="writeBtn" type="submit">글쓰기</button>			
					</div>
				</form>
						
			</div>				
	
		<div id="boardArea">
		                
		  <form action="${pageContext.servletContext.contextPath}/board/detail" method="get">
		  	<div><input id="hiddenPar" type="hidden" name="par" value=""></div>
		  	<div><input id="titleHiddenPar" type="hidden" name="title" value=""></div>		  	
		  			  	
					
			<table id="freeBoard">
				<tr>
						<th>오름차순</th>
						<th>내림차순</th>
					<th>No</th>
					<th>말머리 
						<select id="categorySelect" onchange="selectCategory(this.value);">
							<option class="defaultOption">------선택------</option>							
					<c:forEach items="${categoryList}" var="category" varStatus="status">
							
							<option value="${category.categoryName}" class="optionList${fn:length(categoryList)-status.index}"><c:out value="${category.categoryName}"/> </option>
					</c:forEach>
						</select>
					</th>
					
					<th>제목</th>
					<th>글쓴이</th>
					<th>조회수</th>
					<th>작성시간</th>
				</tr>
				
				
				
				<c:forEach items="${boardList}" var="board" varStatus="status">             
				<tr>
						<td><c:out value="${status.index+1 }"/></td>
						<td><c:out value="${fn:length(boardList)-status.index}"/></td>
					<td><c:out value="${board.postNo}" /></td>           
					
					<td><c:out value="${board.categoryName }"/></td>  
										
					<td><button class="titleBtn" data-par="${board.postNo}"><c:out value="${board.postTitle}"/></button>
									
					</td>
					<td><c:out value="${board.userNo}" /></td>
					<td><c:out value="${board.postViews}" /></td>
					<td><c:out value="${board.postDate}" /></td>
				</tr>
				</c:forEach>
			</table>
			
		</form>
		</div>



	
			<div id="pagingArea">
			
				<div class="pagingBtn" align="center">
				<button id="startPage">◀◀</button>
		
				<c:if test="${ pageInfo.pageNo == 1 }">
					<button disabled>◁</button>
				</c:if>
				<c:if test="${ pageInfo.pageNo > 1 }">
					<button id="prevPage">◁</button>
				</c:if>
		
				<c:forEach var="p" begin="${ pageInfo.startPage }" end="${ pageInfo.endPage }" step="1">
				<c:if test="${ pageInfo.pageNo eq p }">
					<button disabled><c:out value="${ p }"/></button>
				</c:if>
				<c:if test="${ pageInfo.pageNo ne p }">
					<button onclick="pageButtonAction(this.innerText);"><c:out value="${ p }"/></button>
				</c:if>
				</c:forEach>
		
				<c:if test="${ pageInfo.pageNo == pageInfo.maxPage }">
					<button disabled>▷</button>	
				</c:if>
				<c:if test="${ pageInfo.pageNo < pageInfo.maxPage }">
					<button id="nextPage">▷</button>
				</c:if>
		
				<button id="maxPage">▶▶</button>
				</div>
			
				
			</div>
			

			
	
	
	</div> <!-- 게시판 영역 섹션 종료 -->

		<div id="footer">
			
		</div>
		
		<script>
		
		/*select태그 option선택시 값을 가져오는 함수*/
		const selectCategory=(par)=>{
			
			if(par=='------선택------'){
				
				par = '';				
				
			}
			
			$.ajax({
				
				type:"POST",
				url:"${pageContext.servletContext.contextPath}/board/searchByCategory",
				data:{
					categoryName : par
				},
				
				success : function(data){
					
					
					
				}
				
				
			});
			
			
			
		};
		
		
		
	 	/* 비로그인시 글쓰기 접근 불가 */
	 	const id = '${loginMember}';		 	
	 	
	 		
		function loginCheck(){
			
			if(id==""){
				
				
				alert("로그인이 필요합니다.");
				return false;
			} 
			return true;
		}	
		
		
		
		/*말머리 추가하는 팝업*/
		const openPopup = ()=>{

				
			/*비로그인시에는 팝업 열지 않도록 분기 형성 (loginCheck 함수 재사용)*/
				if(!(loginCheck())){
									
					return;			
				} 
				
				
				document.open("${pageContext.servletContext.contextPath}/board/category", 
		        		"new", "toolbar=no, menubar=no, scrollbars=yes, resizable=no, width=950, height=700, left=0, top=0" 
						);
				
			}
			
		
		 	const $titleBtns = document.querySelectorAll('.titleBtn');
		 	
		 	const $hiddenPar = document.querySelector('#hiddenPar');

		 	const $titleHiddenPar = document.querySelector('#titleHiddenPar');		 	
		 	
		 	
		 	for(const $titleBtn of $titleBtns){
		 		
		 		$titleBtn.addEventListener('click', ()=>{
		 			
		 			let parNo = $titleBtn.dataset.par;	
		 			
		 			$hiddenPar.setAttribute('value', parNo); 
		 			
		 			$titleHiddenPar.setAttribute('value', $titleBtn.textContent)					
		 			
		 			
		 		})
		 		
		 	}
		 	
		 	
	 	
		 	
			/**/
			const link = "${ pageContext.servletContext.contextPath }/board";
			
			function pageButtonAction(text) {
				location.href = link + "?currentPage=" + text ;
			}
			
			if(document.getElementById("startPage")){
				const $startPage = document.getElementById("startPage");
				$startPage.onclick = function(){
					location.href = link + "?currentPage=1";
				}
			}
			
			if(document.getElementById("maxPage")){
				const $maxPage = document.getElementById("maxPage");
				$maxPage.onclick = function(){
					location.href = link + "?currentPage=${ pageInfo.maxPage }";
				}
			}
			
			if(document.getElementById("prevPage")){
				const $prevPage = document.getElementById("prevPage");
				$prevPage.onclick = function(){
					location.href = link + "?currentPage=${ pageInfo.pageNo - 1 }";
				}
			}
			
			if(document.getElementById("nextPage")){
				const $nextPage = document.getElementById("nextPage");
				$nextPage.onclick = function(){
					location.href = link + "?currentPage=${ pageInfo.pageNo + 1 }";
				}
			}
		 	
		</script>
		
</body>
</html>