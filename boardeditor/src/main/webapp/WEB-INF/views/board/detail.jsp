<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
		#header{		
		width:1920px;
		height:180px;
		
		}	
	
		#section{
		border : solid 1px gray;
		height : 1000px;
		}
		
		#boardArea{
   	    display: block;
   	   
   	    
   		}
   		
   		#postArea{
   		display: flex;
   	    justify-content:center;
   	    align-itmes:center;  		  		
   		}
   		
   		#postContents{
   			resize:none;
   		}
		
		#buttonArea{
   		display: flex;
   	    justify-content:center;
   	    align-itmes:center;
 	  	}
 	  	
 	  	#centerBtn{
 	  		padding-left:50px;
 	  		padding-right:10px;
 	  	}
 	  	
 	  	#editor{
 	  	
 	  		width:900px;
 	  		height:300px;
 	  	}
		
		#fileAtag:hover{
   		color:purple;
   		}   
   		
   		#fileAtag:visited{
   		color:purple;   		
   		}
   		
   		
        .resizable{
            display : inline-block;
            resize:both;
            overflow:hidden;
            line-height: 0;
        }

        .resizable img {
            width: 100%;
            height: 100%;
            }
	
		
		#commentArea{
					
		
		}

		#commentEditor{
			width: 900px;
			height: 100px;
		
		}
		
		.commentContainer{
			margin-left:100px;
			padding-left:100px;
			width: 900px;
			height: 100px;
		}
		
   		
.ql-snow .ql-Table-Input .ql-picker-options {
    padding: 3px 5px;
    width: 152px;
}

.ql-snow .ql-Table-Input .ql-picker-item {
    border: 1px solid transparent;
    float: left;
    height: 16px;
    margin: 2px;
    padding: 0px;
    width: 16px;
    background: lightsteelblue;
}

.ql-snow .ql-Table-Input .ql-picker-item:hover, .ql-snow .ql-Table-Input .ql-picker-item.ql-picker-item-highlight {
    border-color: #000;
    background: steelblue;
}

.ql-snow .ql-Table-Input.ql-picker {
    width: 28px;
}

.ql-snow .ql-Table-Input.ql-picker .ql-picker-label {
    padding: 2px 4px;
}
   		



</style>

<!-- jQuery (for quill-better-table)  -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


<!-- Quill editor (+ better-table) -->

<script src="https://cdn.quilljs.com/2.0.0-dev.3/quill.js"></script>
<script src="https://unpkg.com/quill-better-table@1.2.8/dist/quill-better-table.min.js"></script>
<link type="text/css" href="https://cdn.quilljs.com/2.0.0-dev.3/quill.snow.css" rel="stylesheet">
<link type="text/css" href="https://unpkg.com/quill-better-table@1.2.7/dist/quill-better-table.css" rel="stylesheet"/>


<!-- KaTex (formula) -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js"></script>

<!-- bootStrap -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>


<!-- font-awesome (CustomButton Icon) -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">



<!-- axios -->

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>
<body>

	<div id="header">
		
		<jsp:include page="../common/header.jsp"/>
	
	</div>
	
	
	<c:set var="par" value = "${par}"/>
	
	<div id="section">
	
		<div id="boardArea">
		 	
		 	
		 	<form method="POST" name="form" enctype="multipart/form-data">
        	
	        <div>
	            <input id="hiddenLoginMember" type="hidden" value="${loginMember}">
	            <input id="hiddenPar" type="hidden" name="par" value="${par}">
	            <input type="hidden" name="userNo" value="${loginMember.userNo}">	            
	        </div>

		        <table id="postArea">
		        	
		        	<tr>
		        		<td>말머리</td>
		        		
		        		
		        	</tr>
		        	
		        	<tr>
		        		
		        		<td>
		        		
		        			<select name="categoryName">
			        				<option>말머리 선택</option>
	
		        			<c:forEach items="${boardList}" var="board">
							
							<c:if test="${loginMember.userNo!=board.userNo}">
								
							</c:if>
								
		        			<c:if test="${empty categoryList && (loginMember.userNo==board.userNo)}" >
			        				<option>말머리가 없어요</option>
			        				<option>말머리를 만들어주세요</option>
		        			</c:if>	
		        					        			
		        			
		        			<c:if test="${!empty categoryList && (loginMember.userNo==board.userNo)}"> 
		        			
			        			<c:forEach items="${categoryList}" var="category">
			        				<option value="${category.categoryName}" <c:if test="${category.categoryName == board.categoryName}">selected</c:if>  >
			        				   <c:out value="${category.categoryName}"/>
			        				</option>
			        			</c:forEach>
		        			</c:if>	
		        			</c:forEach>
		        			</select>
		        		</td>	
		        		
		        	</tr>
		        	
		            <tr>
		                <td>제목</td> 
		            </tr>
		            <c:choose>	
	        			<c:when test="${fn:contains(par,'POST')}">

							<tr>
		            		    <td><input id="postTitle" type="text" name="postTitle" value="${title}" ></td>
				            </tr>
		            		        			
	                     </c:when>
		        

						 <c:otherwise>
				
				            <tr>
				                <td><input type="text" name="postTitle"></td>
				            </tr>
		            
		            	</c:otherwise>
		            </c:choose>
		            
		                
		            <tr>
		                <td>내용</td>
		            </tr>
		            
		            
		            
		            
		            <tr>
		                <td>
		                	<div id="editor">
		                		
		                	</div>
		                	
		                	<div id="inputArea">
		                	
		                	</div>
		                	
		                	<div id="fileArea">
		                	<c:forEach var="file" items="${boardFile}">
		                
		                <c:forEach items="${boardList}" var="board">
		                
		                <c:choose>
		                
		                
							<c:when test="${loginMember.userNo eq board.userNo}">
							
		                	
								<button type="button" onclick="fileDelete('${par}')"> X </button>
							
							
							</c:when>

		                
		                </c:choose>
		                </c:forEach>
							
								<a id="fileAtag" href='javascript:void(0);' onclick="fileDown('${par}')">
								<c:out value="${file.fileRename}${file.fileExtension}"/>
								</a>		                	
		                	</c:forEach>
		                	</div>
		                </td>
		            </tr>
		            
		            
		            
		            
		            <c:choose>	
	        			<c:when test="${fn:contains(par,'POST')}">
		            <tr>
		            
		            	<c:forEach items="${boardList}" var="board">		            	
		                <td>
		                <input type="hidden" id="postContents" name="postContents" value="${board.postContents}">
		                <input type="hidden" id="postDelta" name="postDelta" value="${board.postDelta}">
		                <input type="hidden" id="postNo" name="postNo" value="${board.postNo}">
		                <input type="hidden" id="userNoCheck" name="userNo" value="${board.userNo}">
		                </td>
		                
		                </c:forEach>
		                
		            </tr>
						</c:when>
					
					
					
					<c:otherwise>
		            <tr>
		                <td>
		                <input type="hidden" id="postContents" name="postContents" value="${board.postContents}">
		                <input type="hidden" id="postDelta" name="postDelta" value="${board.postDelta}">
		                </td>
		            </tr>
					</c:otherwise>
					</c:choose>
		
		
		        </table>
		        <div id="buttonArea">
		        
		        	<table>
		        		<tr>
		        			<td><button class="btn btn-secondary" onclick="javascript: form.action='${pageContext.servletContext.contextPath}/board'">뒤로</button></td>
		        			<td id="centerBtn">
		        			
		        			
		        			
		        		<c:forEach items="${boardList}" var="board">	
		        			<input id="hiddenBoardUserNo" type="hidden" value="${board.userNo}">
		        			
		        		
		        			<c:if test="${fn:contains(par,'POST') && (board.userNo == loginMember.userNo ) }">		        				
								<button class="btn btn-primary" type="submit" onclick="javascript: form.action='${pageContext.servletContext.contextPath}/board/modify'">수정</button>
								<button class="btn btn-danger" type="submit" onclick="javascript: form.action='${pageContext.servletContext.contextPath}/board/delete'">삭제</button>
		        			
		        			</c:if>
		        			
		        			<c:if test="${par != 'write' && !empty loginMember}">		        				
		        			
		        			
		        			
		        			</c:if>
						</c:forEach>
		        			
																        			
		        			</td>
		        			
		        			
		        		</tr>
		        		
		        		<tr>
		        			<td>
		        				
		        			
		        			</td>
		        		</tr>
		        		
		        	</table>
		        	
		        	
				</div>
				
    		
    		</form>
				
				<div class="mt-4" id="commentArea">
   					
   					<div id="commentPosition"></div>
   					
   					<div id="commentEditor">
   					
   					</div> 
   					
   					<div class="float-right">
   					<button class="btn btn-primary pull-right" type="button" onclick="addComment();">등록</button>
   					</div>
   					<!--  
   					<button type="button" onclick="addComment();">덧글 달기 test</button>
   					-->
   				  				
   				</div>

	
		</div> <!-- boardArea 끝 -->
	

	
	
	</div> <!-- section 끝 -->
	
	<script>
	

	
	let files = '${boardFile}'
	
	if(files=='[]'){
		
		files = '';		

	}	
	
	
	
	const $fileArea = document.querySelector('#fileArea');
	
	const $id = document.querySelector('#hiddenLoginMember'); 
		
	const id = $id.value;	
	
	const $hiddenPar = document.querySelector('#hiddenPar');
	
	const par = $hiddenPar.value;
	
	const $postTitle = document.querySelector('#postTitle');
	
	const $postContents = document.querySelector('#postContents');
	const $postDelta = document.querySelector('#postDelta');
	
	
	const $commentArea = document.querySelector('#commentArea');
	const $commentEditor = document.querySelector('#commentEditor');
	const $commentPosition = document.querySelector('#commentPosition');
		
	let delta = ${postDelta};
	
	
	
	const form = document.querySelector('form');
	
	
	/*Quill editor*/
	const $editorForm = document.querySelector('#editor');
	var curQuillDiv = $("#editor");
	
	
	
		Quill.register({
		  'modules/better-table': quillBetterTable		  
		}, true);
			
		var tableOptions = [];
		
		var maxRows = 7;
		var maxCols = 7;
		
		for (let r = 1; r <= maxRows; r++) {
			  for (let c = 1; c <= maxCols; c++) {
			    tableOptions.push('newtable_' + r + '_' + c);
			  }
		}
		
		
		
		const imageHandler = ()=>{
			
			
			const input = document.createElement('input');
			
			input.setAttribute('type', 'file');
			input.setAttribute('accept', 'image/*');
			input.click();
			
			input.addEventListener('change', (e)=>{
				
				var files = e.target.files;
				
				var fileReader = new FileReader();
				
				fileReader.readAsDataURL(e.target.files[0]);
				
				const range = quill.getSelection();
				
				fileReader.onload = function(e){
					
					
					quill.insertEmbed(range, 'image', e.target.result);
													
				}								
			});						
		}
		
		
		
	     /*툴바 항목 모듈*/
	var toolbarOptions = [
		
		[{ 'header': [1, 2, 3, 4, 5, 6, false]}],        
        [{ 'color': [] }, 'bold', 'italic', 'underline', 'strike'],        
        [{ 'align': [] }],
        ['blockquote', 'code-block'],        
        [{'list': 'ordered'}, {'list': 'bullet'}, {'list': 'check'}],
        ['image'],
		['fileUpload'],
		['formula'],		
        ['clean'],
        [{'Table-Input': tableOptions}]        
        
	]
	
	     /*로그인시 툴바 표시 모듈*/
	     
	var curQuill = new Quill(curQuillDiv[0], options);    
	     
	     
	var options = {
			modules: {	
			    toolbar: {
			    	container :toolbarOptions,
			    	handlers:{
			    		'image': imageHandler
			    	}
			    },			    
			    table: false,
			    'better-table': {
			      operationMenu: {
			        items: {
			          unmergeCells: {
			            text: 'Unmerge cells'
			          }
			        },
			        color: {
			          colors: ['green', 'red', 'yellow', 'blue', 'white'],
			          text: 'Background Colors:'
			        }
						}
					},
			    keyboard: {
			      bindings: quillBetterTable.keyboardBindings
			    }
			  },			 
			  theme: 'snow',
			  readOnly : false
	}
   
	
	     /* 비로그인시 툴바 비활성화 모듈*/
	var othersOptions = {
			modules: {
			    toolbar: false
		  	},
		  	theme: 'snow',
		  	readOnly : true			
	}
	

	     /* 파일 업로드용 커스텀 버튼 생성 */
	const customBtnMake = ()=>{
				
		const $inputArea = document.querySelector('#inputArea');
		
		const customBtn = document.querySelector('.ql-fileUpload');
		
		const icon = document.createElement('i');
		
		icon.setAttribute('class','fas fa-file-upload');
		
		customBtn.append(icon);			
	
		
		
		/*업로드 파일 확장자 체크*/
	const checkFile=(f)=>{
			
			var file = f.files;
			
			if(!/\.(gif|jpg|jpeg|png|mp4|wma|mp3|ogg|mkv|pdf|hwp|doc|docx|xls|xlsx|txt|csv|ppt|zip|7z|rar|alz)$/i.test(file[0].name)) {
				alert('허용된 확장자만 가능합니다.\n\n현재 파일 : ' + file[0].name)
			} // 체크를 통과했다면 종료.
			else {
				return		
			};
			
			f.outerHTML = f.outerHTML;
			
		};
		
		
		/* 파일 첨부는 한개만 가능하도록 flag 설정*/
		var count = 0;

		customBtn.addEventListener('click', function(){
			
			if(files!=''){
				
				return alert('파일은 한개만 첨부 가능!!')
			}
			
			if(count == 0){
				
				var input = document.createElement('input');
				
				var acceptUploadType = 'image/*,video/*,audio/*,.pdf,.hwp,.doc,.docx,.xls,.xlsx,.txt,.csv,.ppt,.zip,.7z,.rar,.alz';
				
				input.setAttribute('type','file');
				input.setAttribute('accept', acceptUploadType)
				input.setAttribute('id','uploadFile');
				input.setAttribute('name','uploadFile');
				
				$inputArea.append(input);
				
				input.click();
				
				count++;
				
			} else {
				
				alert('파일은 한개만 첨부 가능');
			}
			
			
		})
		
	};

	
	/*Quill editor 객체 생성 (위의 모듈 활용)*/
	
	
	
	/*덧글 달기 test*/

	var commentEditor = new Quill($commentEditor, options);
	
	const $postNo = document.querySelector('#postNo');
	
	var postNo = $postNo.value;
	
	const $userNoCheck = document.querySelector('#userNoCheck');
	
	var userNoCheck = $userNoCheck.value;
	
	var addCommentCount = 0;
	
	
	
	
	const addReply = ()=>{
		
		
		console.log("test");
		
	}
	
	
	
	
	
	const addComment = ()=>{
		
		console.log(commentEditor.getContents());
		
		var commentDelta = commentEditor.getContents();		
		
		
		$.ajax({
			type : "POST", 
			url: "${pageContext.servletContext.contextPath}/board/comment/add",
			contentType:"application/json;charset=UTF-8",
			data:JSON.stringify({
				
				postNo : postNo,
				userNo : userNoCheck,
				commentDelta : commentDelta
				
			}),
			
			success:function(data){
				
				var commentDiv = document.createElement("div");
				commentDiv.setAttribute('class', 'comment');
				
				var commentUl = document.createElement("ul");				
				var commentLi = document.createElement("li");
				
				var modifyBtn = document.createElement("button");
				modifyBtn.innerHTML = "수정";

				var deleteBtn = document.createElement("button");
				deleteBtn.innerHTML = "삭제";
				
				var replyBtn = document.createElement("button");
				replyBtn.innerHTML = "답글쓰기";
				replyBtn.setAttribute("onclick","addReply();"); 
				
				
				var comment = document.querySelectorAll('.comment');
				
				console.log(comment); 
				
				
				/* if(comment.length>0){
					
					return;
				} */
										
					//commentDiv.innerHTML = data;
					commentDiv.innerHTML = "commentDiv";
					$commentPosition.before(commentDiv);
					commentDiv.append(commentUl);
					commentUl.append(commentLi);
					commentLi.append(modifyBtn);
					commentLi.append(deleteBtn);
					commentLi.append(replyBtn);
					
					addCommentCount++;
					
				}
		});
		
		
		
		
	}
	
	
	
	
	
	var divNum = 0;
	
	var count = 1;
	
    const addCommentDiv=()=>{
        
        count = divNum+1;
        
        divNum = count;
		
        if(count>1){
        	
        	alert("댓글 생성은 댓글 하나당 하나에만 가능");
        	return;
        }
        
        /*댓글란을 생성한다. id를 부여해서 quill 객체 구현할 준비*/
        var div = document.createElement('div');
        
        div.setAttribute('id','editorContainer' + count);
        div.setAttribute('class','commentContainer');       //댓글은 별도의 클래스로 CSS(높이 다르게)를 적용.

        /*body에 블록으로 추가되도록 한다.*/
        $commentArea.append(div);
        
        /*댓글란들마다 부여한 번호대로 객체를 생성한다. */    

        var quill = new Quill('#editorContainer'+count, {
            modules: {
            toolbar: [
                [{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline'],
                ['image', 'code-block']
            ]
            },                
            placeholder: '이곳은 댓글란입니다.',
            theme: 'snow'  // or 'bubble'
        });   
                                 
    }
	
	
	
		/* 비로그인 상태*/	
		if(id==''){
			
			$postTitle.setAttribute('readOnly', true);
			var quill = new Quill('#editor', othersOptions);
			
			quill.setContents(delta);		
		
		/*로그인 상태*/
		} else if (id!=''){
			
			
			/* 로그인 + 기존 작성 글을 눌렀을 때 */	
			if (par!=""){ 
				
				$hiddenBoardUserNo = document.querySelector('#hiddenBoardUserNo');
				
				const BoardUserNo = $hiddenBoardUserNo.value;
				
				
				/* 작성자와 로그인된 회원이 같을 경우 */
				if( (BoardUserNo == '${loginMember.userNo}' ) ) {		
					
					var Block = Quill.import('blots/block');	                    
                    Block.tagName = 'DIV';
                    Quill.register(Block, true); 
					
					var quill = new Quill(curQuillDiv[0], options);
					
					
					/*에디터 내부의 텍스트 변화를 감지한다. 사진 이동시에도 감지 가능.*/
				    quill.on('text-change', function(){
						
				    		
				        /* 다른 div로 옮겨도 resizable 가능하게 만듦 */
				        let $div = document.querySelectorAll('div');

				            for(var i = 0 ; i<$div.length; i++){

				                var item = $div.item(i);
				                $(item).find('img').parent().attr("class","resizable");
				                //img태그를 포함한 div에 resizable 클래스 속성 부여 -> 사진의 시각적 리사이즈
				              					                
				                $(item).find('br').parent().removeAttr("class");                
            				    // img 태그가 없는 div는 resizable 클래스를 없앰 (여백기호의 속성만 남겨야함)
				                            				    
				            }
				         
				            
				    });   
					
					
					customBtnMake();
					
					/*delta객체로 에디터 내부의 정보들을 가져와 넣는다. (이후로 저장된 글의 정보들 로딩)*/
					quill.setContents(delta);
					
					var curTableIconSpan = curQuillDiv.parent().find('span.ql-Table-Input.ql-picker')[0].childNodes[0];
					curTableIconSpan.innerHTML = "<svg style=\"right: 4px;\" viewbox=\"0 0 18 18\"> <rect class=ql-stroke height=12 width=12 x=3 y=3></rect> <rect class=ql-fill height=2 width=3 x=5 y=5></rect> <rect class=ql-fill height=2 width=4 x=9 y=5></rect> <g class=\"ql-fill ql-transparent\"> <rect height=2 width=3 x=5 y=8></rect> <rect height=2 width=4 x=9 y=8></rect> <rect height=2 width=3 x=5 y=11></rect> <rect height=2 width=4 x=9 y=11></rect> </g> </svg>";
					var curTableCellIconSpans = $(curTableIconSpan.parentNode.childNodes[1]).children();
					curTableCellIconSpans.click((function(){
					  var curQuillBetterTable = quill.getModule('better-table');
					  var curQuillToolbar = quill.getModule('toolbar');
					  return function() {
					    var curRowIndex = Number(this.dataset.value.substring(9).split('_')[0]);
					    var curColIndex = Number(this.dataset.value.substring(9).split('_')[1]);
					    curQuillBetterTable.insertTable(curRowIndex, curColIndex);
					    // The following two lines have been added, thinking that it would fix the issue of keeping the icon in blue color.  However Quill keeps adding the classes back, so this fix doesn't work.
					    $(this).parent().parent().find(".ql-selected").removeClass("ql-selected");     	 $(this).parent().parent().find(".ql-active").removeClass("ql-active");
					  };
					})());
					curTableCellIconSpans.hover(function(){
					  var curRowIndex = Number(this.dataset.value.substring(9).split('_')[0]);
					  var curColIndex = Number(this.dataset.value.substring(9).split('_')[1]);
					  $(this).parent().children().each((function() {
					    var curRowIndex1 = curRowIndex;
					    var curColIndex1 = curColIndex;
					    return function() {
					      var curRowIndex2 = Number(this.dataset.value.substring(9).split('_')[0]);
					      var curColIndex2 = Number(this.dataset.value.substring(9).split('_')[1]);
					      if (curRowIndex2 <= curRowIndex1 && curColIndex2 <= curColIndex1) {
					        $(this).addClass("ql-picker-item-highlight");
					      }
					    };
					  })());


					}, function(){
					  $(this).parent().children().removeClass("ql-picker-item-highlight");
					});  
					
					
				/* 타인의 글을 눌렀을 경우*/
				} else {
					$postTitle.setAttribute('readOnly', true);
					var quill = new Quill('#editor', othersOptions);				
					quill.setContents(delta);
					
				} 
				
			} 
			
			
			
			
			////사진 리사이즈 임시 중단 ///
			
/* 			
			var ImgSizeObj = function(width, height){
				
				this.width = width;
				this.height = height;				
			}
			
			var ImgObjArray = [];
			
			
 	        var canvas=document.createElement("canvas");
	        var ctx=canvas.getContext("2d");
	        var cw=canvas.width;
	        var ch=canvas.height;
	        var maxW=32;
	        var maxH=32; 
			
			
			//form 태그의 정보들을 submit하기 이전에 처리
			
				
				
				var width ="";
				var height = "";
				
				// (for resize) 현재 첨부된 사진들의 높이 너비를 가져온다.
				const $resizeDiv = document.querySelectorAll('div[class=resizable]');
					
	
				for(var i = 0 ; i<$resizeDiv.length ; i++){
					
					var item = $resizeDiv.item(i);
					
					var width = window.getComputedStyle(item).width;
					var height = window.getComputedStyle(item).height;
					
					
					imgSizeObj = new ImgSizeObj(width, height);
					
					ImgObjArray.push(imgSizeObj);
					// 객체를 생성해 사진 사이즈를 넣고 객체를 다시 배열에 넣음.	
					
				}  
				
					//console.log(ImgObjArray);
					
				
					
					
				///사진의 물리적 리사이즈 
				
				
				const $img = document.querySelectorAll('img');
				
				
				
 				for(var i = 0; i<$img.length; i++){
					
					var item = $img.item(i);
					
  				var maxW = ImgObjArray[i].width;
					var maxH = ImgObjArray[i].height;
					
					 item.onload = function(){
			            var iw=item.width;
			            var ih=item.height;
			            var scale=Math.min((maxW/iw),(maxH/ih));
			            var iwScaled=iw*scale;
			            var ihScaled=ih*scale;
			            canvas.width=iwScaled;
			            canvas.height=ihScaled;
			            ctx.drawImage(img,0,0,iwScaled,ihScaled);
						
					}	 		
					
					console.log(canvas.toDataURL("image/png", 1.0));
					
					const range = quill.getSelection(); 
 					//item.remove();
					//quill.insertEmbed(range, 'image', "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAIQAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIACkAQAMBEQACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAIBgcECQEFCgL/xAA5EAAABgECBAMECAUFAAAAAAABAgMEBQYRByEACBMxEhVBIjJRYQkUFjM0cXKBI0KxssI3dYPB0f/EABsBAAICAwEAAAAAAAAAAAAAAAUGBAcCAwgB/8QAPBEAAgECBAMFBgMFCQEAAAAAAQIDBBEFEiExABNBBlFhcYEUIjKhsfAjQpEHM3LBwggWNDVSc3SCtdH/2gAMAwEAAhEDEQA/APJS407kUidRNBQcZx4S99sjnOwY29NvjgA46CjkZeu240uO/wCdyOhHCKKyNx8Qvfr3+Q+Wvh04jjiBfoDgUjgJRHAiUd/mO3Ye+B27h+U6KVTpfU/e32dNuvHoIbUG9+u/H23aPC7CkbYd/Z27ZHAhnbcAH8wHfYQm51INj4dNz39PPjXJoL+B37/v7BOslYQb10JfYOIGDsBTb9sjv++N+3qO4BjnsLAd29z5+n310gNKLnv7yfH1JHjfi9NJ+WTV3XKdeVbSWgWTUCxR1dm7W9h63GrST9GArzT65KSBWyQdRYUymRatGiAKvpWUeR8LEtXsvIsGLkfXYnS0CLJVzxwRvLFCryHKpklbIijwJJLMSFRA0jlY0LD2HmTuY4UMrhGcqgJYIgzMbDuGg/MWKqAWIBQ8T9HPzFrVqSuJ9PFHFbhdL6frHMyreZgVUIuhXyQWi6q/eECU6xJGVctnBk4AiSk6iggqs5j0kyicF+ftJh3MaL2m0gqpqPIUlBNRAqvKtygGRFZfxbiK5AVyTbjyWKdInnEeeNKaKsd1ZMqwTsyROfeGsjI4VB75yMctgTxBx5TZ96kJyxbgg4EolMkcB93Bexe2R9AHG3qGeIcuMwA6SJ11Ljb5jX07rcQ4MQW/z20G1r92p+7niEy/KXYEOoPly2A2AekYOw4Efd3z6/LsGO+AxmFvzr+o+ex6+mm3EwYoiDVgDuNNOutgfDS+1xrobPCB5eQm44BIxA5hLkBAm/YAHf4b9hEfj6CIEpsQjjaxNtd7iwA87X/XXQW1A4rmnxuUspDnXXQ9x07wfK2ouBx0clygOl1jAmwOOR2wmGfntjAjnOPiOcCI8a1xWIbkeeYW1PQi/wBe/u0aKHGi1gzC5Heb+ot43/nx1B+SyZD2041UwbDjojjcREQ2Lj5ZEcbCIZ7hNixZNLt8xtbTY7aE66WB4KzYgjIWDDoemvQePh9ba8M7k++iimOYKcl2s1My+nsJHGbsI20LUZ7Za3I2NUpnXkEpJtpaILAG8tRO6LIrkeMSuFI6MdHZSE3BoyI3GO1UeHCFY1hqJZGBeH2lYZUiJIEqxmNxKuZWUrdCD7wJUOVww6jqcaeVI5JaZFzLHU+yvPTNMoVzTySJLGIX5bZ1JupBykh2jV/XJy0cpdC01baS3qb09pVf1t04psXorqy/qjBds/ulFY1+KgayZu7hY+IXkEHUtRqN9lpORj5EsNT2MlR0jRCUVJuY2sMVrJZkqoDLJLTVFQ1bEk8juYJnldmZCc9lZJZQ4UWZzmJBJAfcPoWj9jqJEgirYKc0dW0IvHURhFF1sItebDDJG0iMyRtJDluQ6ROf0+oqmnF9pVhUO1jbheLOlc1FJFEpGVdp9xcwLOAapolRkkWj5SopizVcqLSCjo00LfBjMSIr8JzCSRpGDGaR2bNsYssF8zH3LrGBe5ObM18xBE+tpoailkppfcgZVElrRjlLKJnTPoVRgGVmBUpGcyMjKCBtqxoxC6jSvnsTUEIevVetIQEDGIsUGhWlbgzSDpEybIpCKI+Mzx0qVuRMxzeMpW6ZCGRbJhJ63EY2lkhgkSmGaTM7qjsF0eVo2IkUsBmsyBsoF7nThHxajrK6SWrp6BoKCkpuVCXEdNempxI5kEEjRyJGbuyLyw3LyDLfQDux6BVQxHBTMG4HLkogBCZz7u2ADPpjA49OwcDU7S1ANhIR1vmPkTbp6738uK/qasKDlbXpr4+mgseo7z4HPRmLjFmaBTJpCPhKGDAGPEYMb/LfAiO+dgzkB4uvEql1ZtTvYX6Hc28RbQ6Dxvsi0rlWsT9T028tPXhQtanBAJFFG6Idv5SDv8Mhv3DAdgzuIAAhwAOISg6Mb7m+mnTYCwGnXfu4NQ1BVlN7EX/TT06a/Pa/F007T6rzIESBs3MYQ2wRPIiIY32yG4bZAc43xxqkxeoiG7aXtqfX5G4v6DvZqaUToADsQLX+9d76AC4HD45ZqvprphMPDyi15bSM2sgVFhVWS8vGSbVuiobou6/GxUjIPXBOq4UXODdcCNUydHoCDrrh58WlqZVBa7KCF/DZ3sNWAsGuvUgiwtc68O/ZqSjoWdZHrxPUPbkwI1RTTIq3W8EUUkwlW7lmA+ECzgZl4WmuEnfmWleoErpZHmtNkYUiaLT4ewQrpVSVeOI14p5G5TRZFdrs3xkW6KkAjFOlG7xokKcYdVMEiAu183aCPsr2hk7PIr4/FgmKTYJEI4J5JMUShmahVaepqIKV5GqxEqQ1M9JTyyFYZpoo2ZltHB1oJcRw729njw6StpFrnYSoY6N5VE5IVOcpWHmMbI8ybhCwsda1Ld6pyttuQXJZs5boWx3IxqLVm9dxrGLn4WvzhyvJhJygjPW4bFJ2VtPScdGwrVMrVNuMW8VFxcbfzp/Z4xj9sWKt2rT9pCdomwuIYbJhFZ2qwWjwPExjcldjy9oKKhpKbBuz8lTgENJFgVVh1XNRVCvLW1ZgrYkkPZ7s8+/tBo+yVM+Ft2ZekLyJULWR0VZNWwCKJ41pJHeaeoaOokUSCVOZc5FcqMwkltiW1lqGnUW5Vk3NrgHyKRSNpUlQVkmvmCkehIIoxj93BMavLrh1kkwSGbdsCvwWZPHBCtHpEekqueKmhlzvOJMvuy+zSOgkZWKcp2RaVmIX4eYSlwGCEG1KY52iosLjnjnlxCkmEVo54sLnmg5kiM8SxVU0Aw6eY5D+Caq6n99yxdhq/l9ShkXDty4dGXWcrqrrrLKCoqsqocx1VVlDmMc6pzGE6ihjCYxjGExhERHit3zlmZiWZiWZmJJYkkklt7k6km5JJ7+Oe56mWokkkkd3eV2eSR2LSSO7Fnd2NyWckliSSSTcm54FuhcyJ27XKoAPs5ATfL1xvnccAAZDHrnAdJ4otnbvO+h3IOvXcWtsO7c3iQ0hGp0v9/p3i977dbKabsRGLIpwWyJSAOxhDJvDjGBHAAYBHAb43EchnIGGHPJYg+o6fPQX8R46C290KAHUeo08dNuM7TjWhtGSJSquiB4DgAgY5Q2Ac4H03EMDvnYBwAZ43VeGloyQNhobb7228L66DWw30JYfU8ttSQDbTYet/Tpppre92tTObOHpr4y72w+QxjtJsaTlmkYnOSiSEUsL4rGLZOQcxoKShiixcqPmKqQoKAArMVCpyDRW9nmp5tFOXNcjKTe24LAqcpvbU9fdykk8O+C9p6fDp3FVUrT0jIZJJFpjNUSNCrNHTQsA6R89nAdpVaMIgUNAz85XhU9fKvqIzj3K87DRRX03WqeK68+1tR6zY7xOxLWDZzLJmpGojbINlYK6pPVoyZ2MfZ7KWGqzsCVtrIuJTNI4a62CRtIqhL+/fKj23bIwBdmUgpYq11kBtDD8SpqymFVzIVVp44ionjkeFp2i9npqh0LRJVyLPAWp42JilnEAaYcqaTO5gJ+v0mSgLJJP2jAtgVhYx42dlTSbhKzartFqksdNA6aab5Rm4I5VVV8CKgsyNkTvHjc4an95I2WykLYC1wQD7h13uS4Y6ABPh943mzVUFPyxPKsYmnSnQsRl50isyRsdkZ8oCFrZi6i/vKDp256uaOV0rUqjnTbVV9XHre4SlQ1c0XsVRpNgY2io2+qWJxB2laVtdRsoqlazxqmCEdFzgsJ2oWMhFmUJIxK0aeFVvPKsK08mQPIIJ4jFGySrN+GpzPG2oLr7uYqy6WU3HFfdrsblpkmp8Nr+VWJKtPW4TV0MNVHJTyyBpZw88M0Qjko5GflmR0dGhUxQstRFJpwkuYxuiVQ4PgAS9v4oDtgc4KAiHfOfkABsGeIf915XNih23IHrrc27/HpxVq0rG3u220y9AOnp32v114gmk2sCUaVFM7gAEoAGPEIGxgN+/wANwHYBH4gO144lQZwbAWN9vD/5f7041tHkGoOh22+9bju28uEdPau+YRwgmv4hFMMYMOByAgUA3z+/p3EOFyGn5clzpY72tax6308LW07tLcQKmTLcDoPPXf08dNNL6cHVxqbJs5AyiThQo9TYAMbAYEcfzb/MPX0Htg4eSYTmsDYa7G36kkA6ne2nEWKZgSdtb/r0t+vcDc7a8SNzq/LPGQFVeLY8IjucewB6ZEdg39RHv3DGF+aCN5dALkncDXz6a7bbjXXjGeYyEr08/lpfY9x39OMGtc5urOj72nlq9kOav0zU+D1dbU191fsxPXCvva89ZKWtvHOI2SnI0TVaIRCPXlUyMCpuXUMpGSLty+OQp8EhqkkuGVpIGhMiGzBGDfBmzIpBYm+TU2BuLDgrhGJ4lQimip5z7LTYhDii0r35EtXCYSrzrGY3kUiCJcnMAAGZMrkvxK7z9JhrBqnEanRVyeV5ZrqfqU01PlRbMXia8FIR8OhXI+uVly5lHizOox9fja7DMo2TUl5BtH1aFSSlSmGTPJ4T9l4UEapmPLTlC5BJG+aQDKC5NyWXKCzObWy8H6rHMTr4KynnMbpXV64jKAhzRzLHyljhZmZkgWJYo1RszKkMShwM+cN6x8xdr1LfRDOesk5Zn6a8RHRh5KSfSjluzj3XWj4pkLldVRJokqc5UWSJioAddUemHWUE0KrwRaSjEyrlyVuGA2AFs+I0yG5N73z63BsLmwvx7DFVVTvUVUstRItNIivPI8pVUiZY1zSEsAgHuAaD8tzYcAmT1dk1yqF+tKD4s4DxmAB75zgcdxyI4Ac/DcAdkwqIEWUDpsO++gt1Pp6W4ZYcMQWOUk+VhuN9vXTS4IO3Dfpv35Pz/wAT8Sa74PQ/z4Sanr9/6eFA0/AB+gf7Q4UpfjPr9TwAqt28z9V4gMh+ML+sP6jxg3wJ5/1DjQnwj1+p45P9wb9/7T8Qk/ej+I/z41P8fof6eKEu/v8A7h/lw54VsfIfQ8F6D8n33cVYb7pX9X/vBOf4B/EPoeDyfGv8K/UcQuD/ANRap/vzX+g8L3aL/JZ/+Thf/r0PB2H/AAs3+0304IK3vf8AH/0HDfwz/mX/AK/Qcf/Z");
					//quill.insertEmbed(range, 'image', canvas.toDataURL("image/png", 1.0) );
				} 

 				
				e.preventDefault();	
				
				
				*/
					
			////사진 리사이즈 임시 중단 ///
				
			form.onsubmit = function(e){							 
							 
							 
				var postDelta = document.querySelector('input[name=postDelta]');
				
				postDelta.value = JSON.stringify(quill.getContents());
				
	
			}
		
			
		} //Quill editor 끝
		
		
		
	/* 첨부 파일 삭제 axios*/	
		
	const fileDelete = (par)=>{
		
		if(  !(confirm('정말 삭제하시겠습니까?')) ){
						
			return;
		}
		
		
		 const deleteUrl = "${pageContext.servletContext.contextPath}/board/fileDelete";
		
		axios.post(deleteUrl, {
				      par : par
				    },			
				    
		).then(response=>{
			
			$fileArea.remove();
			
			files = '';
			
			
		}).catch(error=> {
          console.log(error);
        });
		
	};//첨부파일 삭제 axios 끝
	
	
	/* 첨부 파일 다운로드*/	
	
	const fileDown = (par)=>{
		
		let downForm = document.createElement('form');
		downForm.setAttribute('id','downForm');
		
		let parInput = document.createElement('input');
		parInput.setAttribute('type','hidden');
		parInput.setAttribute('name','par');		
		parInput.setAttribute('value', par);
		
		downForm.append(parInput);
		
		downForm.setAttribute('action', "fileDownload");
		downForm.setAttribute('method', "post");
		
		$fileArea.append(downForm);
		
		downForm.submit();			
		
	}
	
	
		
	</script>
	
	
</body>
</html>