<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>header</title>

<style>
	#headerTable td, #loginTable td{
		border:0px;
	}

	#tableArea{	
   	    display: flex;
   	    justify-content:center;
   	    align-items:center;   
	}
	#title{
   	    text-align: center;
   	    color:blue;   		
    }
    
    #loginForm{
    	padding-left: 600px; 
    	display: flex;
    	justify-content:center;
   	    align-items:center; 
    }    
    
    
    
</style>

<!-- bootStrap -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

</head>
<body>
		<h1 id="title">board Editor Test </h1>
		<div>
		
		</div>
		<div id="tableArea">
			<table id="headerTable">
				<tr>
					<td><button class="btn btn-info" onclick="location.href='${ pageContext.servletContext.contextPath }/'">홈으로</button></td>
					<td><button class="btn btn-info" onclick="location.href='${ pageContext.servletContext.contextPath }/board'">자유게시판</button></td>
					<td><button class="btn btn-info" onclick="location.href='${ pageContext.servletContext.contextPath }/member/regist'">회원가입</button></td>
					
					
						<c:if test="${loginMember.accessPermission eq 'ADMIN'}">
							
							<td><button class="btn btn-info" onclick="location.href='${ pageContext.servletContext.contextPath }/admin/boardAdmin'">게시판 관리</button></td>
								
						</c:if>
											
				</tr>
			</table>

		</div>
		
			<div id="loginForm">
				
				<c:if test="${ empty loginMember }">
			
				<form action="${pageContext.servletContext.contextPath}/login" method="post">	
				<table id="loginTable">
					<tr>
						<td>ID</td>
						<td><input type="text" name="userId"></td> 
						<td rowspan="2"><button>로그인</button></td>
					</tr>
					<tr>
						<td>비밀번호</td>
						<td><input type="password" name="userPwd" required></td>
					</tr>
					
				</table>		
				</form>	
				
				</c:if>
				
				<c:if test="${ !empty loginMember }">
				
				<c:out value="${ loginMember.userId }님  환영합니다."/>
				
				<button class="mx-3 btn btn-secondary" onclick="location.href='${pageContext.servletContext.contextPath}/logout'">로그아웃</button>
				
				</c:if>
				
			</div>
</body>
</html>